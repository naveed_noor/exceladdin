﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpirationProjectExpenseExcelWorkbook
{
    public class SAPLogInSession
    {
        private SAPLogInSession()
        {
            TanentID = AppConstants.TestTenantID;
        }

        private static SAPLogInSession _SAPLogInSession;
        public static SAPLogInSession Instance
        {
            get
            {
                if (_SAPLogInSession == null)
                    _SAPLogInSession = new SAPLogInSession();
                return _SAPLogInSession;
            }
        }

        public Boolean IsLoggedIntoSAP { get; set; }
        public String TanentID { get; set; }
        public String Username { get; set; }
        public String Password { get; set; }

        public void StoreUserCredentials(String _TanentID, String _Username, String _Password)
        {
            Username = _Username;
            Password = _Password;
            TanentID = _TanentID;
        }
    }

}
