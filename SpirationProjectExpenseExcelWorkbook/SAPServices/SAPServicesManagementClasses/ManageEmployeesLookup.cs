﻿using SpirationProjectExpenseExcelWorkbook.SAPServices.ObjectManupilationClasses;
using SpirationProjectExpenseExcelWorkbook.SAPServices.SAPWebServiceClasses;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpirationProjectExpenseExcelWorkbook.SAPServices.SAPServicesManagementClasses
{
    public class ManageEmployeesLookup
    {

        private String EmployeesFilePath = String.Empty;
        
        public List<String> GetEmployeesStringList(List<SAPEmployeesData> EmployeesDatalist)
        {
            List<String> CodeStringList = new List<string>();
            EmployeesDatalist.Sort((x, y) => x.GivenName.CompareTo(y.GivenName));

            foreach (SAPEmployeesData item in EmployeesDatalist)
                CodeStringList.Add(String.Format("{1} {2} - {0}", item.EmployeeID, item.GivenName, item.FamilyName));
            return CodeStringList;
        }

        public String FindEmployeeInList(List<SAPEmployeesData> EmployeesDatalist, String EmployeeId)
        {
            foreach (SAPEmployeesData item in EmployeesDatalist)
            {
                if (item.EmployeeID.Equals(EmployeeId))
                    return String.Format("{1} {2} - {0}", item.EmployeeID, item.GivenName, item.FamilyName);
            }
            return String.Empty;
        }

        public ManageEmployeesLookup()
        {
            string path = Path.GetTempPath();
            EmployeesFilePath = string.Format("{0}\\Employees.txt", path);
        }

        public List<SAPEmployeesData> PullEmployees(String EmployeeID)
        {
            List<SAPEmployeesData> SAPEmployeesDataList = ReadFile(EmployeesFilePath);

            if (SAPEmployeesDataList == null)
            {
                SAPEmployeesDataList = QueryEmployeesFromSAP.GetEmployeesData(EmployeeID);

                if (SAPEmployeesDataList != null)
                    WriteFile(SAPEmployeesDataList, EmployeesFilePath);
            }
            return SAPEmployeesDataList;
        }

        public void RefreshEmployeesListFromSAP(String EmployeeID)
        {
            List<SAPEmployeesData> SAPEmployeesDataList = QueryEmployeesFromSAP.GetEmployeesData(EmployeeID);
            if (SAPEmployeesDataList != null)
                WriteFile(SAPEmployeesDataList, EmployeesFilePath);
        }

        private void WriteFile(List<SAPEmployeesData> CodesList, String FileName)
        {
            try
            {
                File.Delete(FileName);
            }
            catch (Exception exp)
            {
            }

            using (Stream stream = File.Open(FileName, FileMode.Create))
            {
                var bformatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

                bformatter.Serialize(stream, CodesList);
            }
        }

        private List<SAPEmployeesData> ReadFile(String FileName)
        {
            try
            {
                using (Stream stream = File.Open(FileName, FileMode.Open))
                {
                    var bformatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                    List<SAPEmployeesData> CodesList = (List<SAPEmployeesData>)bformatter.Deserialize(stream);
                    return CodesList;
                }
            }
            catch (Exception FNFexp)
            {
                return null;
            }
        }

    }
}
