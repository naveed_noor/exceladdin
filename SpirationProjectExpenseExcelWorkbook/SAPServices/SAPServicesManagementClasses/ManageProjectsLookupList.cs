﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SpirationProjectExpenseExcelWorkbook.SAPServices.SAPServicesManagementClasses
{
    public class ManageProjectsLookupList
    {
        
        private String FilePath = String.Empty;

        public List<String> GetProjectsStringList(List<ProjectsList> ServiceProductsDatalist)
        {
            if (ServiceProductsDatalist == null) return null;

            List<String> CodeStringList = new List<string>();
            ServiceProductsDatalist.Sort((x, y) => x.ProjectID.CompareTo(y.ProjectID));
            foreach (ProjectsList item in ServiceProductsDatalist)
                CodeStringList.Add(String.Format("{0}", item.ProjectID));
            return CodeStringList;
        }

        public static List<String> GetProjectTasksStringList(List<ProjectsList> ServiceProductsDatalist, String ProjectID)
        {
            if (ServiceProductsDatalist == null) return null;

            List<String> CodeStringList = new List<string>();
            ProjectsList SelectedProjectObj = ServiceProductsDatalist.Find(e => e.ProjectID == ProjectID);
            if (SelectedProjectObj == null) return null;
            foreach (ProjectTasksList item in SelectedProjectObj.ProjectTasksList)
                CodeStringList.Add(String.Format("{0}", item.ProjectElementID));
            return CodeStringList;
        }

        public static List<DateTime> GetProjectTasksDates(List<ProjectsList> ServiceProductsDatalist, String ProjectID, String ProjectTaskID)
        {
            if (ServiceProductsDatalist == null) return null;

            List<DateTime> CodeStringList = new List<DateTime>();
            ProjectsList SelectedProjectObj = ServiceProductsDatalist.Find(e => e.ProjectID == ProjectID);
            if (SelectedProjectObj == null) return null;
            
            foreach (ProjectTasksList item in SelectedProjectObj.ProjectTasksList)
            {
                if(item.ProjectElementID == ProjectTaskID)
                {
                    CodeStringList.Add(item.EarliestStartDate);
                    CodeStringList.Add(item.LatestEndDate);
                    break;
                }
            }
            return CodeStringList;
        }

        public String FindProjectInList(List<ProjectsList> ServiceProductsDatalist, String ProjectID)
        {
            foreach (ProjectsList item in ServiceProductsDatalist)
            {
                if (item.ProjectID.Equals(ProjectID))
                    return String.Format("{0}", item.ProjectID);
            }
            return String.Empty;
        }

        public ManageProjectsLookupList()
        {
            string path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            FilePath = string.Format("{0}\\ProjectsList.txt", path);
        }

        public List<ProjectsList> PullProjects(String ProjectID)
        {
            List<ProjectsList> SAPServiceProductsDataList = ReadFile(FilePath);

            if (SAPServiceProductsDataList == null)
            {
                SAPServiceProductsDataList = QueryProjectFromSAP.GetProjectsList(ProjectID);

                if (SAPServiceProductsDataList != null)
                    WriteFile(SAPServiceProductsDataList, FilePath);
            }
            return SAPServiceProductsDataList;
        }

        public void RefreshProjectsListFromSAP(String ProjectID)
        {
            List<ProjectsList> SAPServiceProductsDataList = QueryProjectFromSAP.GetProjectsList(ProjectID);
            if (SAPServiceProductsDataList != null)
                WriteFile(SAPServiceProductsDataList, FilePath);
        }

        private void WriteFile(List<ProjectsList> CodesList, String FileName)
        {
            try
            {
                File.Delete(FileName);
            }
            catch (Exception exp)
            {
            }

            using (Stream stream = File.Open(FileName, FileMode.Create))
            {
                var bformatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

                bformatter.Serialize(stream, CodesList);
            }
        }

        private List<ProjectsList> ReadFile(String FileName)
        {
            try
            {
                using (Stream stream = File.Open(FileName, FileMode.Open))
                {
                    var bformatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                    List<ProjectsList> CodesList = (List<ProjectsList>)bformatter.Deserialize(stream);
                    return CodesList;
                }
            }
            catch (Exception FNFexp)
            {
                return null;
            }
        }

    }

    [Serializable()]
    public class ProjectsList
    {
        public String ProjectID { get; set; }
        
        public String ProjectUUID { get; set; }

        public List<ProjectTasksList> ProjectTasksList = new List<ProjectTasksList>();
    }

    [Serializable()]
    public class ProjectTasksList
    {
        public String ProjectElementID { get; set; }

        public String ProjectTaskUUID { get; set; }

        public DateTime EarliestStartDate { get; set; }
        public DateTime LatestEndDate { get; set; }

    }

}
