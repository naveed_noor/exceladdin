﻿using SpirationProjectExpenseExcelWorkbook.SAPServices.ObjectManupilationClasses;
using SpirationProjectExpenseExcelWorkbook.SAPServices.SAPWebServiceClasses;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpirationProjectExpenseExcelWorkbook.SAPServices.SAPServicesManagementClasses
{
    public class ManageServiceProductsLookup
    {
        
        private String FilePath = String.Empty;

        public List<String> GetServiceProductsStringList(List<ServiceProductsData> ServiceProductsDatalist)
        {
            List<String> CodeStringList = new List<string>();

            ServiceProductsDatalist.Sort((x, y) => x.Description.CompareTo(y.Description));

            foreach (ServiceProductsData item in ServiceProductsDatalist)
                CodeStringList.Add(String.Format("{1} - {0}", item.InternalID, item.Description));
            return CodeStringList;
        }

        public String FindServiceProductInList(List<ServiceProductsData> ServiceProductsDatalist, String ServiceProductId)
        {
            foreach (ServiceProductsData item in ServiceProductsDatalist)
            {
                if (item.InternalID.Equals(ServiceProductId))
                    return String.Format("{1} - {0}", item.InternalID, item.Description);
            }
            return String.Empty;
        }

        public ManageServiceProductsLookup()
        {
            string path = Path.GetTempPath();
            FilePath = string.Format("{0}\\ServiceProducts.txt", path);
        }

        public List<ServiceProductsData> PullServiceProducts(String ServiceProductId)
        {
            List<ServiceProductsData> SAPServiceProductsDataList = ReadFile(FilePath);

            if (SAPServiceProductsDataList == null)
            {
                SAPServiceProductsDataList = QueryServiceProductsFromSAP.GetServiceProductsData(ServiceProductId);

                if (SAPServiceProductsDataList != null)
                    WriteFile(SAPServiceProductsDataList, FilePath);
            }
            return SAPServiceProductsDataList;
        }

        public void RefreshServiceProductsListFromSAP(String ServiceProductId)
        {
            List<ServiceProductsData> SAPServiceProductsDataList = QueryServiceProductsFromSAP.GetServiceProductsData(ServiceProductId);
            if (SAPServiceProductsDataList != null)
                WriteFile(SAPServiceProductsDataList, FilePath);
        }

        private void WriteFile(List<ServiceProductsData> CodesList, String FileName)
        {
            try
            {
                File.Delete(FileName);
            }
            catch (Exception exp)
            {
            }

            using (Stream stream = File.Open(FileName, FileMode.Create))
            {
                var bformatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

                bformatter.Serialize(stream, CodesList);
            }
        }

        private List<ServiceProductsData> ReadFile(String FileName)
        {
            try
            {
                using (Stream stream = File.Open(FileName, FileMode.Open))
                {
                    var bformatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                    List<ServiceProductsData> CodesList = (List<ServiceProductsData>)bformatter.Deserialize(stream);
                    return CodesList;
                }
            }
            catch (Exception FNFexp)
            {
                return null;
            }
        }

    }
}
