﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;
using SpirationProjectExpenseExcelWorkbook.SAPServices.SAPServicesManagementClasses;

namespace SpirationProjectExpenseExcelWorkbook
{
    public class ManageSAPCodeList
    {
        private String GLCodesFilePath = String.Empty;
        private String CurrencyCodeFilePath = String.Empty;
        private String QuantityTypeCodeFilePath = String.Empty;

        public const String  GeneralLedgerAccountAliasCodeDataTypeName = "GeneralLedgerAccountAliasCode";
        public const String CurrencyCodeDataTypeName = "CurrencyCode";
        public const String QuantityTypeCodeName = "QuantityTypeCode";
        private const String GLNameSpace = "http://sap.com/xi/AP/Common/GDT";
        private const String CurrencyNameSpace = "http://sap.com/xi/AP/Common/GDT";
        private const String QuantityTypeCodeNameSpace = "http://sap.com/xi/AP/Common/GDT";

        public List<String> GetCodesStringList(List<SAPCodesListClass> Codeslist)
        {
            List<String> CodeStringList = new List<string>();
            foreach (SAPCodesListClass item in Codeslist)
                CodeStringList.Add(String.Format("{0} - {1}", item.Value, item.Description));
            return CodeStringList;
        }

        public String FindCodeListString(List<SAPCodesListClass> Codeslist, String CodeName)
        {
            foreach (SAPCodesListClass item in Codeslist)
            {
                if (item.Value.Equals(CodeName))
                    return String.Format("{0} - {1}", item.Value, item.Description);
            }
            return String.Empty;
        }


        public ManageSAPCodeList()
        {
            string path = Path.GetTempPath();
            GLCodesFilePath = string.Format("{0}\\GeneralLedgerAccountAliasCodes.txt", path);
            CurrencyCodeFilePath = string.Format("{0}\\CurrencyCodes.txt", path);
            QuantityTypeCodeFilePath = string.Format("{0}\\QuantityTypeCode.txt", path);
        }

        public void PullAllCodesFromSAP()
        {

            PullCodesFromSAP(GeneralLedgerAccountAliasCodeDataTypeName, GLNameSpace, GLCodesFilePath);


            PullCodesFromSAP(CurrencyCodeDataTypeName, CurrencyNameSpace, CurrencyCodeFilePath);

            PullCodesFromSAP(QuantityTypeCodeName, QuantityTypeCodeNameSpace, QuantityTypeCodeFilePath);
        }

        public void RefreshAllCodesFromSAP()
        {
            RefreshCodeListFromSAP(GeneralLedgerAccountAliasCodeDataTypeName, GLNameSpace, GLCodesFilePath);

            RefreshCodeListFromSAP(CurrencyCodeDataTypeName, CurrencyNameSpace, CurrencyCodeFilePath);

            RefreshCodeListFromSAP(QuantityTypeCodeName, QuantityTypeCodeNameSpace, QuantityTypeCodeFilePath);
        }

        public List<SAPCodesListClass> GetGeneralLedgerAccountAliasCodes()
        {
            return PullCodesFromSAP(GeneralLedgerAccountAliasCodeDataTypeName, GLNameSpace, GLCodesFilePath);
        }

        public List<SAPCodesListClass> GetCurrencyCodes()
        {
            return PullCodesFromSAP(CurrencyCodeDataTypeName, CurrencyNameSpace, CurrencyCodeFilePath);
        }

        public List<SAPCodesListClass> GetQuantityTypeCodes()
        {
            return PullCodesFromSAP(QuantityTypeCodeName, QuantityTypeCodeNameSpace, QuantityTypeCodeFilePath);
        }

        private List<SAPCodesListClass> PullCodesFromSAP(String CodeName, String CodeDataType, String FileName)
        {
            List<SAPCodesListClass> CodesList = ReadFile(FileName);            
            
            if (CodesList == null)
            {
                CodesList = QueryCodeList.GetSAPQueryCodesList(CodeName, CodeDataType);

                if (CodesList != null)
                    WriteFile(CodesList, FileName);
            }
            return CodesList;
        }

        private void RefreshCodeListFromSAP(String CodeName, String CodeDataType, String FileName)
        {
            List<SAPCodesListClass> CodesList = QueryCodeList.GetSAPQueryCodesList(CodeName, CodeDataType);
            if (CodesList != null)
                WriteFile(CodesList, FileName);
        }

        private void WriteFile(List<SAPCodesListClass> CodesList, String FileName)
        {
            try
            {
                File.Delete(FileName);
            }
            catch (Exception exp)
            {
            }

            using (Stream stream = File.Open(FileName, FileMode.Create))
            {
                var bformatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

                bformatter.Serialize(stream, CodesList);
            }
        }

        private List<SAPCodesListClass> ReadFile(String FileName)
        {
            try
            {
                using (Stream stream = File.Open(FileName, FileMode.Open))
                {
                    var bformatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                    List<SAPCodesListClass> CodesList = (List<SAPCodesListClass>)bformatter.Deserialize(stream);
                    return CodesList;
                }
            }
            catch (Exception FNFexp)
            {
                return null;
            }
        }


    }
}
