﻿using SpirationProjectExpenseExcelWorkbook.SAPServices.ObjectManupilationClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpirationProjectExpenseExcelWorkbook.SAPServices.SAPWebServiceClasses
{
    public class QueryServiceProductsFromSAP
    {
        public static List<ServiceProductsData> GetServiceProductsData(String ProjectID)
        {
            SAPQueryServiceProductIn.ServiceProductByElementsResponseMessage_sync Response = CallWebService(ProjectID);

            if (Response.ServiceProduct == null)
                return null;
            
            List<ServiceProductsData> ServiceProductsDataList = new List<ServiceProductsData>();

            foreach (SAPQueryServiceProductIn.ServiceProductbyElementsResponseMaterial_sync ProductsDataObject in Response.ServiceProduct)
            {
                ServiceProductsData _ServiceProductsData = new ServiceProductsData();

                _ServiceProductsData.InternalID = ProductsDataObject.InternalID.Value;
                foreach (var item in ProductsDataObject.Description)
                {
                    if (item.Description != null)
                    {
                        _ServiceProductsData.Description = item.Description.Value;
                    }
                }

                ServiceProductsDataList.Add(_ServiceProductsData);
            }
            return ServiceProductsDataList;
        }

        internal static SAPQueryServiceProductIn.ServiceProductByElementsResponseMessage_sync CallWebService(String ProductID)
        {
            //Create the Web Service reference service object
            SAPQueryServiceProductIn.service svc = new SAPQueryServiceProductIn.service();
            //Set Credentials
            SAPLogInSession _SAPLogInSession = SAPLogInSession.Instance;
            String uName = _SAPLogInSession.Username;
            String uPassword = _SAPLogInSession.Password;

            _SAPLogInSession.Username = AppConstants.InternalSAPUserName;
            _SAPLogInSession.Password = AppConstants.InternalSAPPassword;

            svc.Credentials = new SAPCredentials();

            // Set the URL to the Specified URL in the Application Prefrences Setting Object
            //svc.Url = svc.Url.Replace("my335110", "?????");

            //Create Request object
            SAPQueryServiceProductIn.ServiceProductByElementsQueryMessage_sync request = new SAPQueryServiceProductIn.ServiceProductByElementsQueryMessage_sync();

            //Initialise the instance of 'CustomerSelectionByElements' object on the request
            request.ServiceProductSelectionByElements = new SAPQueryServiceProductIn.ServiceProductByElementsQuerySelectionByElements();

            SAPQueryServiceProductIn.ServiceProductByElementsQuerySelectionByDescription[] Param = new SAPQueryServiceProductIn.ServiceProductByElementsQuerySelectionByDescription[1];

            Param[0] = new SAPQueryServiceProductIn.ServiceProductByElementsQuerySelectionByDescription();

            //Include
            Param[0].InclusionExclusionCode = "I";
            //Equals
            Param[0].IntervalBoundaryTypeCode = "1";

            Param[0].LowerBoundaryDescription = new SAPQueryServiceProductIn.SHORT_Description();
            Param[0].LowerBoundaryDescription.Value = ProductID;

            //Initialise the instance with your Parameters
            request.ServiceProductSelectionByElements.SelectionByDescription = Param;

            //Initialise the instance of 'Processing Conditions' object on the request
            request.ProcessingConditions = new SAPQueryServiceProductIn.QueryProcessingConditions();

            //set processing conditions
            request.ProcessingConditions.QueryHitsMaximumNumberValueSpecified = false;
            request.ProcessingConditions.QueryHitsUnlimitedIndicator = true;

            try
            {
                //Make the call, passing the request
                SAPQueryServiceProductIn.ServiceProductByElementsResponseMessage_sync response = svc.FindByElements(request);
                _SAPLogInSession.Username = uName;
                _SAPLogInSession.Password = uPassword;

                return response;
            }
            catch (Exception exp)
            {
                _SAPLogInSession.Username = uName;
                _SAPLogInSession.Password = uPassword;
                if (exp.Message.ToString().Contains("The remote name could not be resolved") == true)
                {
                    throw new Exception("Network Issue and Can't reach to the SAP ByDesign Server");
                }
            }
            return null;
        }


    }
}
