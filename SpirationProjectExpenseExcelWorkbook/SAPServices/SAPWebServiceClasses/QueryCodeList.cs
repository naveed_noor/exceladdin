﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpirationProjectExpenseExcelWorkbook
{
    public class QueryCodeList
    {
    
        public static List<SAPCodesListClass> GetSAPQueryCodesList(String CodeTypeName, String NamespaceURI)
        {
            SAPQueryCodeListIn.CodeListByIDResponseMessage_sync response = CallWebService(CodeTypeName, NamespaceURI);
            List<SAPCodesListClass> _SAPCodesList = new List<SAPCodesListClass>();

            if (response != null && response.CodeList != null && response.CodeList.Length >= 1)
            {
                foreach (SAPQueryCodeListIn.CodeListByIDResponse item in response.CodeList)
                {
                    foreach (SAPQueryCodeListIn.CodeListByIDResponseCode item2 in item.Code)
                    {
                        SAPCodesListClass Obj = new SAPCodesListClass();
                        Obj.Value = item2.Value;
                        Obj.Description = item2.Description.Value;

                        _SAPCodesList.Add(Obj);
                    }
                }
            }
            return _SAPCodesList;
        }

        private static SAPQueryCodeListIn.CodeListByIDResponseMessage_sync CallWebService(String CodeTypeName, String NamespaceURI)
        {
            //Create the Web Service reference service object
            SAPQueryCodeListIn.service svc = new SAPQueryCodeListIn.service();
            //Set Credentials
            SAPLogInSession _SAPLogInSession = SAPLogInSession.Instance;

            String uName = _SAPLogInSession.Username;
            String uPassword = _SAPLogInSession.Password;

            _SAPLogInSession.Username = AppConstants.InternalSAPUserName;
            _SAPLogInSession.Password = AppConstants.InternalSAPPassword;
            svc.Credentials = new SAPCredentials();


            //Create Request object
            SAPQueryCodeListIn.CodeListByIDQueryMessage_sync request = new SAPQueryCodeListIn.CodeListByIDQueryMessage_sync();

            request.CodeListSelectionByID = new SAPQueryCodeListIn.CodeListByIDQuery();

            request.CodeListSelectionByID.SelectionByCodeDataType = new SAPQueryCodeListIn.CodeListByIDQuerySelectionByCodeDataType();

            request.CodeListSelectionByID.SelectionByCodeDataType.Name = CodeTypeName;
            request.CodeListSelectionByID.SelectionByCodeDataType.NamespaceURI = NamespaceURI;

            request.CodeListSelectionByID.SelectionByLanguageCode = "EN";

            try
            {
                //Make the call, passing the request
                SAPQueryCodeListIn.CodeListByIDResponseMessage_sync response = svc.FindCodeListByID(request);

                _SAPLogInSession.Username = uName;
                _SAPLogInSession.Password = uPassword;

                return response;
            }
            catch (Exception exp)
            {
                _SAPLogInSession.Username = uName;
                _SAPLogInSession.Password = uPassword;
                if (exp.Message.ToString().Contains("The remote name could not be resolved") == true)
                {
                    throw new Exception("Network Issue and Can't reach to the SAP ByDesign Server");
                }
            }
            return null;
        }

    }

     [Serializable()]
    public class SAPCodesListClass 
    {
        public String Value { get; set; }
        public String Description { get; set; }

    }
}
