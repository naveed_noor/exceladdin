﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization.Json;
using System.Windows.Forms;

namespace SpirationProjectExpenseExcelWorkbook
{

    public class CallLogonToSAPWebServices
    {
        public static Boolean LogonToSAP(String TanentID, String UserName, String Password)
        {
            try
            {
                string endPoint = String.Format("https://my{0}.sapbydesign.com/sap/a1s/cd/logon", TanentID);
                var client = new RestClient(endPoint);
                var json = client.MakeRequest();
                
                // Default Sucessfull Response <Response><Identity><UserAccount><ID>K8UC9EO6WNO</ID></UserAccount></Identity></Response>
                if (json.Contains("<Response><Identity><UserAccount><ID>") && json.Contains("</ID></UserAccount></Identity></Response>"))
                    return true;
            }
            catch (Exception exp)
            {
                if (exp.Message.Contains("The remote server returned an error: (401) Unauthorized"))
                    return false;
                else
                    MessageBox.Show(exp.Message, "Error in Authentication with SAP", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return false;
        }

    }

}
