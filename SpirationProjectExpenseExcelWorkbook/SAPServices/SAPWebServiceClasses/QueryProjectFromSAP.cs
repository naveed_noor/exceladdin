﻿using SpirationProjectExpenseExcelWorkbook.SAPServices.SAPServicesManagementClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpirationProjectExpenseExcelWorkbook
{
    public class QueryProjectFromSAP
    {
        public static List<SAPProjectsData> GetProjectData(String ProjectID)
        {
            QueryProject.ProjectByElementsResponseMessage_sync Response = CallWebService(ProjectID);
            
            if (Response.ProjectQueryResponse == null)
                return null;

            ManageSAPCodeList _ManageSAPCodeList = new ManageSAPCodeList();
            _ManageSAPCodeList.PullAllCodesFromSAP();
            List<SAPCodesListClass> GeneralLedgerAccountAliasCodesList = _ManageSAPCodeList.GetGeneralLedgerAccountAliasCodes();
            List<SAPCodesListClass> CurrencyCodesList = _ManageSAPCodeList.GetCurrencyCodes();

            List<SAPProjectsData> SAPProjectsDataList = new List<SAPProjectsData>();

            foreach (QueryProject.ProjectByQueryResponse ResponseProjectQuery in Response.ProjectQueryResponse)
            {
                SAPProjectsData _SAPProjectsData = new SAPProjectsData();

                _SAPProjectsData.ProjectID = ResponseProjectQuery.ProjectID.Value;
                _SAPProjectsData.ProjectUUID = ResponseProjectQuery.UUID.Value;

                foreach (var item in ResponseProjectQuery.ProjectTask)
                {
                    ProjectTask _ProjectTask = new ProjectTask();

                    _ProjectTask.ProjectElementID = item.ProjectElementID.Value;
                    _ProjectTask.ProjectTaskUUID = item.UUID.Value;

                    if (item.TaskName != null && item.TaskName.Length > 0)
                        _ProjectTask.TaskName = item.TaskName[0].Name.Value;
                    _ProjectTask.EarliestStartDate = item.EarliestStartDate.Value;
                    _ProjectTask.LatestEndDate = item.LatestEndDate.Value;

                    if (item.TaskExpense != null)
                    {
                        foreach (var TaskExpenseItem in item.TaskExpense)
                        {
                            ProjectTaskExpense _ProjectTaskExpense = new ProjectTaskExpense();

                            _ProjectTaskExpense.TaskExpenseDescription = TaskExpenseItem.Description != null ? TaskExpenseItem.Description.Value : "";

                            _ProjectTaskExpense.ProjectTaskExpenseUUID = TaskExpenseItem.UUID.Value;

                            _ProjectTaskExpense.GeneralLedgerAccountAliasCode = TaskExpenseItem.GeneralLedgerAccountAliasCode != null ? TaskExpenseItem.GeneralLedgerAccountAliasCode.Value : "";
                            _ProjectTaskExpense.GeneralLedgerAccountAliasCodeLookup = _ManageSAPCodeList.FindCodeListString(GeneralLedgerAccountAliasCodesList, _ProjectTaskExpense.GeneralLedgerAccountAliasCode);

                            _ProjectTaskExpense.PalnnedCostAmount = TaskExpenseItem.PlannedCostsAmount.Value;

                            _ProjectTaskExpense.Currency = TaskExpenseItem.PlannedCostsAmount.currencyCode;
                            _ProjectTaskExpense.CurrencyLookup = _ManageSAPCodeList.FindCodeListString(CurrencyCodesList, _ProjectTaskExpense.Currency);

                            _ProjectTaskExpense.PlannedPeriodStartDate = TaskExpenseItem.PlannedPeriod.StartDateTime.Value;
                            _ProjectTaskExpense.PlannedPeriodEndDate = TaskExpenseItem.PlannedPeriod.EndDateTime.Value;

                            if (TaskExpenseItem.TaskExpensePeriodPlan != null)
                            {
                                foreach (var TaskExpensePeriodPlanItem in TaskExpenseItem.TaskExpensePeriodPlan)
                                {
                                    TaskExpensePeriodPlan _TaskExpensePeriodPlan = new TaskExpensePeriodPlan();
                                    _TaskExpensePeriodPlan.InternalIndicator = TaskExpensePeriodPlanItem.InternalIndicator;
                                    _TaskExpensePeriodPlan.PalnnedCostAmount = TaskExpensePeriodPlanItem.PlannedCostsAmount.Value;

                                    _TaskExpensePeriodPlan.CurrencyCode = TaskExpensePeriodPlanItem.PlannedCostsAmount.currencyCode;
                                    _TaskExpensePeriodPlan.CurrencyLookup = _ManageSAPCodeList.FindCodeListString(CurrencyCodesList, _TaskExpensePeriodPlan.CurrencyCode);

                                    _TaskExpensePeriodPlan.PlannedPeriodStartDate = TaskExpensePeriodPlanItem.PlannedPeriod.StartDateTime.Value;
                                    _TaskExpensePeriodPlan.PlannedPeriodEndDate = TaskExpensePeriodPlanItem.PlannedPeriod.EndDateTime.Value;
                                    _TaskExpensePeriodPlan.UUID = TaskExpensePeriodPlanItem.UUID.Value;
                                    _TaskExpensePeriodPlan.Description = TaskExpensePeriodPlanItem.Description != null ? TaskExpensePeriodPlanItem.Description.Value : "";

                                    _ProjectTaskExpense.taskExpensePeriodPlan.Add(_TaskExpensePeriodPlan);
                                }
                            }
                            _ProjectTask.ProjectTaskExpense.Add(_ProjectTaskExpense);
                        }
                    }
                    _SAPProjectsData.ProjectTask.Add(_ProjectTask);
                }

                SAPProjectsDataList.Add(_SAPProjectsData);
            }
            return SAPProjectsDataList;
        }

        public static List<ProjectsList> GetProjectsList(String ProjectID)
        {
            QueryProject.ProjectByElementsResponseMessage_sync Response = CallWebService(ProjectID);

            if (Response == null || Response.ProjectQueryResponse == null)
                return null;

            ManageSAPCodeList _ManageSAPCodeList = new ManageSAPCodeList();
            _ManageSAPCodeList.PullAllCodesFromSAP();
            List<SAPCodesListClass> GeneralLedgerAccountAliasCodesList = _ManageSAPCodeList.GetGeneralLedgerAccountAliasCodes();
            List<SAPCodesListClass> CurrencyCodesList = _ManageSAPCodeList.GetCurrencyCodes();

            List<ProjectsList> _ProjectsList = new List<ProjectsList>();

            foreach (QueryProject.ProjectByQueryResponse ResponseProjectQuery in Response.ProjectQueryResponse)
            {
                ProjectsList _SAPProjectsData = new ProjectsList();

                _SAPProjectsData.ProjectID = ResponseProjectQuery.ProjectID.Value;
                _SAPProjectsData.ProjectUUID = ResponseProjectQuery.UUID.Value;
                foreach (var item in ResponseProjectQuery.ProjectTask)
                {
                    ProjectTasksList _ProjectTask = new ProjectTasksList();
                    _ProjectTask.ProjectElementID = item.ProjectElementID.Value;
                    _ProjectTask.ProjectTaskUUID = item.UUID.Value;
                    _ProjectTask.EarliestStartDate = item.EarliestStartDate.Value;
                    _ProjectTask.LatestEndDate = item.LatestEndDate.Value;

                    _SAPProjectsData.ProjectTasksList.Add(_ProjectTask);
                }
                _ProjectsList.Add(_SAPProjectsData);
            }
            return _ProjectsList;
        }

        internal static QueryProject.ProjectByElementsResponseMessage_sync CallWebService(String ProjectID)
        {
            //Create the Web Service reference service object
            QueryProject.service svc = new QueryProject.service();
            //Set Credentials
            SAPLogInSession _SAPLogInSession = SAPLogInSession.Instance;
            String uName = "", uPassword = "";
            if (_SAPLogInSession.Username == null)
            {
                _SAPLogInSession.Username = AppConstants.InternalSAPUserName;
                _SAPLogInSession.Password = AppConstants.InternalSAPPassword;
            }
            svc.Credentials = new SAPCredentials();

            //Create Request object
            QueryProject.ProjectByElementsQueryMessage_sync request = new QueryProject.ProjectByElementsQueryMessage_sync();

            //Initialise the instance of 'CustomerSelectionByElements' object on the request
            request.ProjectSelectionByElements = new QueryProject.ProjectByElementsQuerySelectionByElements();

            QueryProject.ProjectSelectionByProjectId[] Param = new QueryProject.ProjectSelectionByProjectId[1];

            Param[0] = new QueryProject.ProjectSelectionByProjectId();

            //Include
            Param[0].InclusionExclusionCode = "I";
            //Equals
            Param[0].IntervalBoundaryTypeCode = "1";

            Param[0].LowerBoundaryProjectID = new QueryProject.ProjectID();
            Param[0].LowerBoundaryProjectID.Value = ProjectID;

            //Initialise the instance with your Parameters
            request.ProjectSelectionByElements.SelectionByProjectID = Param;

            //Initialise the instance of 'Processing Conditions' object on the request
            request.ProcessingConditions = new QueryProject.QueryProcessingConditions();

            //set processing conditions
            request.ProcessingConditions.QueryHitsMaximumNumberValueSpecified = false;
            request.ProcessingConditions.QueryHitsUnlimitedIndicator = true;

            try
            {
                //Make the call, passing the request
                QueryProject.ProjectByElementsResponseMessage_sync response = svc.FindProjectByElements(request);
                if (!uName.Equals(""))
                {
                    _SAPLogInSession.Username = uName;
                    _SAPLogInSession.Password = uPassword;
                }

                return response;
            }
            catch (Exception exp)
            {
                if (!uName.Equals(""))
                {
                    _SAPLogInSession.Username = uName;
                    _SAPLogInSession.Password = uPassword;
                }
                if (exp.Message.ToString().Contains("The remote name could not be resolved") == true)
                {
                    throw new Exception("Network Issue and Can't reach to the SAP ByDesign Server");
                }
            }
            return null;
        }


    }
}
