﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpirationProjectExpenseExcelWorkbook.SAPServices.SAPWebServiceClasses
{
    public class ManageMaterialValuation
    {
        public List<MaterialInfo> QueryMaterialInfo(String MaterialID)
        {
            List<MaterialInfo> MaterialInfoList = new List<MaterialInfo>();
            SAPQueryMaterials.MaterialByElementsResponseMessage_sync response = CallMaterialWebService(MaterialID);
            if(response != null && response.Material != null && response.Material.Length > 0)
            {
                foreach (var item in response.Material)
                {
                    MaterialInfo _MaterialInfo = new MaterialInfo();
                    _MaterialInfo.MaterialID = item.InternalID.Value;
                    _MaterialInfo.MaterialUUID = item.UUID.Value;
                    _MaterialInfo.Description = item.Description != null ? (item.Description.Length > 0 ? item.Description[0].Description.Value : "") : "";
                    _MaterialInfo.BaseMeasureUnitCode = item.BaseMeasureUnitCode;
                    _MaterialInfo.InventoryValuationMeasureUnitCode = item.InventoryValuationMeasureUnitCode;

                    foreach (var ValuationItem in item.Valuation)
                    {
                        ValuationInfo ValuationInfoListObj = new ValuationInfo();
                        ValuationInfoListObj.CompanyID = ValuationItem.CompanyID;
                        ValuationInfoListObj.BusinessResidenceID = ValuationItem.BusinessResidenceID;
                        switch (ValuationItem.LifeCycleStatusCode)
                        {
                            case SAPQueryMaterials.ProductProcessUsabilityLifeCycleStatusCode.Item1:
                                ValuationInfoListObj.LifeCycleStatusCode = 1;
                                break;
                            case SAPQueryMaterials.ProductProcessUsabilityLifeCycleStatusCode.Item2:
                                ValuationInfoListObj.LifeCycleStatusCode = 2;
                                break;
                            case SAPQueryMaterials.ProductProcessUsabilityLifeCycleStatusCode.Item3:
                                ValuationInfoListObj.LifeCycleStatusCode = 3;
                                break;
                        }
                        _MaterialInfo.ValuationInfoList.Add(ValuationInfoListObj);
                    }
                    MaterialInfoList.Add(_MaterialInfo);
                }
            }
            return MaterialInfoList;
        }

        private SAPQueryMaterials.MaterialByElementsResponseMessage_sync CallMaterialWebService(String MaterialID)
        {
            //Create the Web Service reference service object
            SAPQueryMaterials.service svc = new SAPQueryMaterials.service();
            //Set Credentials
            SAPLogInSession _SAPLogInSession = SAPLogInSession.Instance;
            String uName = _SAPLogInSession.Username;
            String uPassword = _SAPLogInSession.Password;

            _SAPLogInSession.Username = AppConstants.InternalSAPUserName;
            _SAPLogInSession.Password = AppConstants.InternalSAPPassword;

            svc.Credentials = new SAPCredentials();

            //Create Request object
            SAPQueryMaterials.MaterialByElementsQueryMessage_sync request = new SAPQueryMaterials.MaterialByElementsQueryMessage_sync();

            //Initialise the instance of 'CustomerSelectionByElements' object on the request
            request.MaterialSelectionByElements = new SAPQueryMaterials.MaterialByElementsQuerySelectionByElements();

            SAPQueryMaterials.MaterialByElementsQuerySelectionByInternalID IDObj = new SAPQueryMaterials.MaterialByElementsQuerySelectionByInternalID();
            IDObj.InclusionExclusionCode = "I";
            IDObj.IntervalBoundaryTypeCode = "1";
            IDObj.LowerBoundaryInternalID = new SAPQueryMaterials.ProductInternalID();
            IDObj.LowerBoundaryInternalID.Value = MaterialID;

            //Initialise the instance with your Parameters
            request.MaterialSelectionByElements.SelectionByInternalID = new SAPQueryMaterials.MaterialByElementsQuerySelectionByInternalID[1];
            request.MaterialSelectionByElements.SelectionByInternalID[0] = IDObj;

            request.ProcessingConditions = new SAPQueryMaterials.QueryProcessingConditions();
            request.ProcessingConditions.QueryHitsUnlimitedIndicator = true;
            request.ProcessingConditions.QueryHitsMaximumNumberValueSpecified = true;

            try
            {
                //Make the call, passing the request
                SAPQueryMaterials.MaterialByElementsResponseMessage_sync response = svc.FindByElements(request);
                _SAPLogInSession.Username = uName;
                _SAPLogInSession.Password = uPassword;

                return response;
            }
            catch (Exception exp)
            {
                _SAPLogInSession.Username = uName;
                _SAPLogInSession.Password = uPassword;
                if (exp.Message.ToString().Contains("The remote name could not be resolved") == true)
                {
                    throw new Exception("Network Issue and Can't reach to the SAP ByDesign Server");
                }
            }
            return null;
        }

        public ResponseMessage CreateMaterialValuationForBusinessResidance(MaterialValuationBusinessResidanceInfo _MaterialValuationBusinessResidanceInfo)
        {
            ResponseMessage _ResponseMessage = new ResponseMessage();
            _ResponseMessage.Res = false;

            //Create the Web Service reference service object
            SAPManageMaterialIn.service svc = new SAPManageMaterialIn.service();

            //Set Credentials
            svc.Credentials = new SAPCredentials();

            //Create Request object
            SAPManageMaterialIn.MaterialMaintainRequestBundleMessage_sync_V1 request = new SAPManageMaterialIn.MaterialMaintainRequestBundleMessage_sync_V1();

            request.Material = new SAPManageMaterialIn.MaterialMaintainRequestBundleMaterial_sync_V1[1];
            request.Material[0] = new SAPManageMaterialIn.MaterialMaintainRequestBundleMaterial_sync_V1();
            request.Material[0].InternalID = new SAPManageMaterialIn.ProductInternalID();
            request.Material[0].InternalID.Value = _MaterialValuationBusinessResidanceInfo.MaterialID;

            request.Material[0].Valuation = new SAPManageMaterialIn.MaterialMaintainRequestBundleValuation[1];
            request.Material[0].Valuation[0] = new SAPManageMaterialIn.MaterialMaintainRequestBundleValuation();
            request.Material[0].Valuation[0].actionCode = SAPManageMaterialIn.ActionCode.Item01;
            request.Material[0].Valuation[0].LifeCycleStatusCode = SAPManageMaterialIn.ProductProcessUsabilityLifeCycleStatusCode.Item1;
            request.Material[0].Valuation[0].CompanyID = _MaterialValuationBusinessResidanceInfo.CompanyID;
            request.Material[0].Valuation[0].BusinessResidenceID = _MaterialValuationBusinessResidanceInfo.BusinessResidenceID;
            try
            {
                //Make the call, passing the request
                SAPManageMaterialIn.MaterialMaintainConfirmationBundleMessage_sync_V1 response = svc.MaintainBundle_V1(request);

                if (response != null && response.Log != null && response.Log.BusinessDocumentProcessingResultCode != null && response.Log.MaximumLogItemSeverityCode.Equals("3"))
                {
                    StringBuilder SB = new StringBuilder();
                    foreach (var LogItem in response.Log.Item)
                    {
                        SB.AppendLine(LogItem.Note);
                    }
                    _ResponseMessage.responseMessage = SB.ToString();
                    return _ResponseMessage;
                }
                else
                    if (response != null && response.Material != null && response.Material.Length > 0 && response.Material[0].InternalID.Value.Equals(_MaterialValuationBusinessResidanceInfo.MaterialID))
                    {
                        _ResponseMessage.Res = true;
                        _ResponseMessage.responseMessage = String.Format("Material '{0}' Valuation Created in SAP for the Business Residence {1} ", _MaterialValuationBusinessResidanceInfo.MaterialID, _MaterialValuationBusinessResidanceInfo.BusinessResidenceID);
                        return _ResponseMessage;
                    }
            }
            catch (Exception exp)
            {
                if (exp.Message.ToString().Contains("The remote name could not be resolved") == true)
                    throw new Exception("Network Issue and Can't reach to the SAP ByDesign Server");

                _ResponseMessage.responseMessage = String.Format("Not Sucessfull due to Exception {0}", exp.Message);
                return _ResponseMessage;
            }
            _ResponseMessage.responseMessage = "Not Sucessfull due to some unknown issue";
            return _ResponseMessage;
        }

        public ResponseMessage MaterialValuationInSAP(ref MaterialValuationObject _MaterialValuationObject)
        {
            ResponseMessage _ResponseMessage = new ResponseMessage();
            _ResponseMessage.Res = false;

            SAPManageMaterialValuationDataIn.service svc = new SAPManageMaterialValuationDataIn.service();
            //Set Credentials
            svc.Credentials = new SAPCredentials();

            SAPManageMaterialValuationDataIn.MaterialValuationDataMaintainRequestBundleMessage_sync request = new SAPManageMaterialValuationDataIn.MaterialValuationDataMaintainRequestBundleMessage_sync();

            request.BasicMessageHeader = new SAPManageMaterialValuationDataIn.BusinessDocumentBasicMessageHeader();
            request.BasicMessageHeader.ID = new SAPManageMaterialValuationDataIn.BusinessDocumentMessageID();
            request.BasicMessageHeader.ID.Value = "1";

            request.MaterialValuationData = new SAPManageMaterialValuationDataIn.MaterialValuationDataMaintainRequestBundle[1];
            request.MaterialValuationData[0] = new SAPManageMaterialValuationDataIn.MaterialValuationDataMaintainRequestBundle();

            request.MaterialValuationData[0].actionCode = SAPManageMaterialValuationDataIn.ActionCode.Item06;
            request.MaterialValuationData[0].actionCodeSpecified = true;

            request.MaterialValuationData[0].valuationPriceListCompleteTransmissionIndicator = false;
            request.MaterialValuationData[0].valuationPriceListCompleteTransmissionIndicatorSpecified = true;
            request.MaterialValuationData[0].ObjectNodeSenderTechnicalID = "1";

            request.MaterialValuationData[0].MaterialInternalID = new SAPManageMaterialValuationDataIn.ProductInternalID();
            request.MaterialValuationData[0].MaterialInternalID.Value = GetFirstPart(_MaterialValuationObject.MaterialID);

            request.MaterialValuationData[0].CompanyID = GetFirstPart(_MaterialValuationObject.CompanyID);

            request.MaterialValuationData[0].ValuationPrice = new SAPManageMaterialValuationDataIn.MaterialValuationDataMaintainRequestBundleValuationPrice[1];
            request.MaterialValuationData[0].ValuationPrice[0] = new SAPManageMaterialValuationDataIn.MaterialValuationDataMaintainRequestBundleValuationPrice();

            request.MaterialValuationData[0].ValuationPrice[0].actionCode = SAPManageMaterialValuationDataIn.ActionCode.Item01;
            request.MaterialValuationData[0].ValuationPrice[0].actionCodeSpecified = true;

            request.MaterialValuationData[0].ValuationPrice[0].ObjectNodeSenderTechnicalID = "2";
            request.MaterialValuationData[0].ValuationPrice[0].PermanentEstablishmentID = GetFirstPart(_MaterialValuationObject.BusinessResidenceID);

            request.MaterialValuationData[0].ValuationPrice[0].ValidityDatePeriod = new SAPManageMaterialValuationDataIn.CLOSED_DatePeriod();
            request.MaterialValuationData[0].ValuationPrice[0].ValidityDatePeriod.StartDate = DateTime.Parse(_MaterialValuationObject.ValidFrom.ToString("yyyy-MM-dd")).Date;
            request.MaterialValuationData[0].ValuationPrice[0].ValidityDatePeriod.EndDate = DateTime.Parse(_MaterialValuationObject.ValidTo.ToString("yyyy-MM-dd")).Date;

            request.MaterialValuationData[0].ValuationPrice[0].PriceTypeCode = new SAPManageMaterialValuationDataIn.PriceTypeCode();
            request.MaterialValuationData[0].ValuationPrice[0].PriceTypeCode.Value = GetFirstPart(_MaterialValuationObject.CostType);

            request.MaterialValuationData[0].ValuationPrice[0].SetOfBooksID = new SAPManageMaterialValuationDataIn.SetOfBooksID();
            request.MaterialValuationData[0].ValuationPrice[0].SetOfBooksID.Value = _MaterialValuationObject.SetOfBooksID;

            request.MaterialValuationData[0].ValuationPrice[0].LocalCurrencyValuationPrice = new SAPManageMaterialValuationDataIn.Price();
            request.MaterialValuationData[0].ValuationPrice[0].LocalCurrencyValuationPrice.Amount = new SAPManageMaterialValuationDataIn.Amount();
            request.MaterialValuationData[0].ValuationPrice[0].LocalCurrencyValuationPrice.Amount.currencyCode = GetFirstPart(_MaterialValuationObject.Currency);
            request.MaterialValuationData[0].ValuationPrice[0].LocalCurrencyValuationPrice.Amount.Value = decimal.Parse(_MaterialValuationObject.Cost.ToString());

            request.MaterialValuationData[0].ValuationPrice[0].LocalCurrencyValuationPrice.BaseQuantity = new SAPManageMaterialValuationDataIn.Quantity();
            request.MaterialValuationData[0].ValuationPrice[0].LocalCurrencyValuationPrice.BaseQuantity.unitCode = GetFirstPart(_MaterialValuationObject.CostUoM);
            request.MaterialValuationData[0].ValuationPrice[0].LocalCurrencyValuationPrice.BaseQuantity.Value = decimal.Parse(_MaterialValuationObject.CostUnit.ToString());

            if (_MaterialValuationObject.AccountDeterminationGroup != null &&
                        _MaterialValuationObject.IsValuationExistsInSAP == true &&
                                _MaterialValuationObject.IsBusinessResidanceCreatedInSAP == true)
            {
                request.MaterialValuationData[0].AccountDeterminationSpecification = new SAPManageMaterialValuationDataIn.MaterialValuationDataMaintainRequestBundleAccountDeterminationSpecification[1];
                request.MaterialValuationData[0].AccountDeterminationSpecification[0] = new SAPManageMaterialValuationDataIn.MaterialValuationDataMaintainRequestBundleAccountDeterminationSpecification();

                request.MaterialValuationData[0].AccountDeterminationSpecification[0].actionCode = SAPManageMaterialValuationDataIn.ActionCode.Item02;
                request.MaterialValuationData[0].AccountDeterminationSpecification[0].actionCodeSpecified = true;

                request.MaterialValuationData[0].AccountDeterminationSpecification[0].ObjectNodeSenderTechnicalID = "3";
                request.MaterialValuationData[0].AccountDeterminationSpecification[0].PermanentEstablishmentID = GetFirstPart(_MaterialValuationObject.BusinessResidenceID);
                request.MaterialValuationData[0].AccountDeterminationSpecification[0].AccountDeterminationMaterialValuationDataGroupCode = new SAPManageMaterialValuationDataIn.AccountDeterminationMaterialValuationDataGroupCode();
                request.MaterialValuationData[0].AccountDeterminationSpecification[0].AccountDeterminationMaterialValuationDataGroupCode.Value = GetFirstPart(_MaterialValuationObject.AccountDeterminationGroup);

                request.MaterialValuationData[0].MaterialFinancialProcessInfo = new SAPManageMaterialValuationDataIn.MaterialValuationDataMaintainRequestBundleMaterialFinancialProcessInfo[1];
                request.MaterialValuationData[0].MaterialFinancialProcessInfo[0] = new SAPManageMaterialValuationDataIn.MaterialValuationDataMaintainRequestBundleMaterialFinancialProcessInfo();

                request.MaterialValuationData[0].MaterialFinancialProcessInfo[0].actionCode = SAPManageMaterialValuationDataIn.ActionCode.Item02;
                request.MaterialValuationData[0].MaterialFinancialProcessInfo[0].actionCodeSpecified = true;

                request.MaterialValuationData[0].MaterialFinancialProcessInfo[0].ObjectNodeSenderTechnicalID = "4";
                request.MaterialValuationData[0].MaterialFinancialProcessInfo[0].PermanentEstablishmentID = GetFirstPart(_MaterialValuationObject.BusinessResidenceID);
                request.MaterialValuationData[0].MaterialFinancialProcessInfo[0].LifeCycleStatusCode = SAPManageMaterialValuationDataIn.ProductProcessUsabilityLifeCycleStatusCode.Item2;
                
            }
            
            try
            {
                svc.SoapVersion = System.Web.Services.Protocols.SoapProtocolVersion.Soap12;
                //Make the call, passing the request
                SAPManageMaterialValuationDataIn.MaterialValuationDataMaintainConfirmationBundleMessage_sync response = svc.MaintainBundle(request);

                if (response != null && response.Log != null && response.Log.BusinessDocumentProcessingResultCode != null && response.Log.MaximumLogItemSeverityCode.Equals("3"))
                {
                    StringBuilder SB = new StringBuilder();
                    foreach (var LogItem in response.Log.Item)
                    {
                        SB.AppendLine(LogItem.Note);
                    }
                    _ResponseMessage.responseMessage = SB.ToString();
                    return _ResponseMessage;
                }
                else
                    if (response != null && response.MaterialValuationData != null && response.MaterialValuationData.Length > 0)
                    {
                        _ResponseMessage.Res = true;
                        _ResponseMessage.responseMessage = String.Format("Material '{0}' Price Created in SAP for the Business Residence '{1}'",
                                                                                _MaterialValuationObject.MaterialID, _MaterialValuationObject.BusinessResidenceID);
                        return _ResponseMessage;
                    }
            }
            catch (Exception exp)
            {
                if (exp.Message.ToString().Contains("The remote name could not be resolved") == true)
                    throw new Exception("Network Issue and Can't reach to the SAP ByDesign Server");

                _ResponseMessage.responseMessage = String.Format("Not Sucessfull due to Exception {0}", exp.Message);
                return _ResponseMessage;
            }

            _MaterialValuationObject.IsPushRequestSentToSAP = true;
            return new ResponseMessage();
        }

        private String GetFirstPart(String Value)
        {
            String str = Value.Substring(0, Value.IndexOf(" - ")).Trim();
            return str;
        }
    }


    public struct ResponseMessage
    {
        public Boolean Res;
        public String responseMessage;
    }
 
}
