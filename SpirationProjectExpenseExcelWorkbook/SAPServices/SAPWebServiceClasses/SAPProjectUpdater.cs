﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpirationProjectExpenseExcelWorkbook
{

    public class ProjectUpdateResponse
    {
        public Boolean IsSucessfull { get; set; }
        public String ProjectID { get; set; }
        public String Response { get; set; }
    }

    public class SAPProjectUpdater
    {

        public static List<ProjectUpdateResponse> UpdateProjectInSAP(List<SAPProjectsData> _SAPProjectsData)
        {
            List<ProjectUpdateResponse> _ProjectUpdateResponseList = new List<ProjectUpdateResponse>();
            foreach (SAPProjectsData SAPProjectsDataItem in _SAPProjectsData)
            {                
                SAPProjectMaintainBundleIn.ProjectMaintainConfirmationBundleMessage_sync response = CallWebService(SAPProjectsDataItem);
                if (response != null && response.Log != null && response.Log.MaximumLogItemSeverityCode != null && response.Log.MaximumLogItemSeverityCode.Equals("3"))
                {
                    ProjectUpdateResponse _ProjectUpdateResponse = new ProjectUpdateResponse();
                    _ProjectUpdateResponse.IsSucessfull = false;
                    _ProjectUpdateResponse.ProjectID = SAPProjectsDataItem.ProjectID;
                    StringBuilder SB = new StringBuilder();
                    foreach (var item in response.Log.Item)
                    {
                        SB.Append(String.Format("Severity Code : {0} and Error Message : {1}{2}",item.SeverityCode , item.Note, Environment.NewLine));
                    }
                    _ProjectUpdateResponse.Response = SB.ToString();
                    _ProjectUpdateResponseList.Add(_ProjectUpdateResponse);
                }
                else if (response != null && response.Project != null && response.Project.Length > 0)
                {
                    ProjectUpdateResponse _ProjectUpdateResponse = new ProjectUpdateResponse();
                    _ProjectUpdateResponse.IsSucessfull = true;
                    _ProjectUpdateResponse.ProjectID = SAPProjectsDataItem.ProjectID;                    
                    _ProjectUpdateResponse.Response = "Sucessfull";
                    _ProjectUpdateResponseList.Add(_ProjectUpdateResponse);
                }
            }
            return _ProjectUpdateResponseList;
        }

        private static SAPProjectMaintainBundleIn.ProjectMaintainConfirmationBundleMessage_sync CallWebService(SAPProjectsData _SAPProjectsData)
        {
            //Create the Web Service reference service object
            SAPProjectMaintainBundleIn.service svc = new SAPProjectMaintainBundleIn.service();
            //Set Credentials
            svc.Credentials = new SAPCredentials();

            SAPProjectMaintainBundleIn.ProjectMaintainRequestBundleMessage_sync request= new SAPProjectMaintainBundleIn.ProjectMaintainRequestBundleMessage_sync();

            request.BasicMessageHeader = new SAPProjectMaintainBundleIn.BusinessDocumentBasicMessageHeader();
            request.BasicMessageHeader.ID = new SAPProjectMaintainBundleIn.BusinessDocumentMessageID();
            request.BasicMessageHeader.ID.Value = "DUMMY";

            request.Project = new SAPProjectMaintainBundleIn.ProjectMessageBundleRequest[1];

            SAPProjectMaintainBundleIn.ProjectMessageBundleRequest ProjectObj = new SAPProjectMaintainBundleIn.ProjectMessageBundleRequest();

            ProjectObj.ActionCode = SAPProjectMaintainBundleIn.ActionCode.Item04;
            ProjectObj.ObjectNodeSenderTechnicalID = "1";
            ProjectObj.ProjectID = new SAPProjectMaintainBundleIn.ProjectID();
            ProjectObj.ProjectID.Value = _SAPProjectsData.ProjectID;
            ProjectObj.UUID = new SAPProjectMaintainBundleIn.UUID();
            ProjectObj.UUID.Value = _SAPProjectsData.ProjectUUID;

            if (_SAPProjectsData.ProjectTask.Count > 0)
            {
                ProjectObj.ProjectTask = new SAPProjectMaintainBundleIn.ProjectTaskMessageBundleRequest[_SAPProjectsData.ProjectTask.Count];
                int i = 0;
                foreach (ProjectTask ProjectTaskItem in _SAPProjectsData.ProjectTask)
                {
                    SAPProjectMaintainBundleIn.ProjectTaskMessageBundleRequest SAPProjectTaskRequestObj = new SAPProjectMaintainBundleIn.ProjectTaskMessageBundleRequest();

                    SAPProjectTaskRequestObj.ActionCode = SAPProjectMaintainBundleIn.ActionCode.Item04;
                    SAPProjectTaskRequestObj.TaskRevenueCompleteTransmissionIndicator = true;
                    SAPProjectTaskRequestObj.TaskRevenueCompleteTransmissionIndicatorSpecified = true;
                    SAPProjectTaskRequestObj.TaskExpenseCompleteTransmissionIndicator = true;
                    SAPProjectTaskRequestObj.TaskExpenseCompleteTransmissionIndicatorSpecified = true;

                    SAPProjectTaskRequestObj.TaskID = new SAPProjectMaintainBundleIn.ProjectElementID();
                    SAPProjectTaskRequestObj.TaskID.Value = ProjectTaskItem.ProjectElementID;
                    SAPProjectTaskRequestObj.UUID = new SAPProjectMaintainBundleIn.UUID();
                    SAPProjectTaskRequestObj.UUID.Value = ProjectTaskItem.ProjectTaskUUID;

                    if (ProjectTaskItem.ProjectTaskExpense.Count > 0)
                    {
                        SAPProjectTaskRequestObj.ProjectTaskExpense = new SAPProjectMaintainBundleIn.ProjectTaskExpenseMessageBundleRequest[ProjectTaskItem.ProjectTaskExpense.Count];
                        int j = 0;
                        foreach (ProjectTaskExpense ProjectTaskExpenseItem in ProjectTaskItem.ProjectTaskExpense)
                        {
                            SAPProjectMaintainBundleIn.ProjectTaskExpenseMessageBundleRequest SAPProjectTaskExpenseObj = new SAPProjectMaintainBundleIn.ProjectTaskExpenseMessageBundleRequest();
                            
                            SAPProjectTaskExpenseObj.TaskExpensePeriodPlanCompleteTransmissionIndicator = true;
                            SAPProjectTaskExpenseObj.TaskExpensePeriodPlanCompleteTransmissionIndicatorSpecified = true;

                            SAPProjectTaskExpenseObj.ExpenseType = new SAPProjectMaintainBundleIn.GeneralLedgerAccountAliasCode();
                            SAPProjectTaskExpenseObj.ExpenseType.Value = ProjectTaskExpenseItem.GeneralLedgerAccountAliasCode;

                            SAPProjectTaskExpenseObj.Description = ProjectTaskExpenseItem.TaskExpenseDescription;

                            if (ProjectTaskExpenseItem.ProjectTaskExpenseUUID != null)
                            {
                                SAPProjectTaskExpenseObj.UUID = new SAPProjectMaintainBundleIn.UUID();
                                SAPProjectTaskExpenseObj.UUID.Value = ProjectTaskExpenseItem.ProjectTaskExpenseUUID;
                                SAPProjectTaskExpenseObj.ActionCode = SAPProjectMaintainBundleIn.ActionCode.Item04;
                            }
                            else
                                SAPProjectTaskExpenseObj.ActionCode = SAPProjectMaintainBundleIn.ActionCode.Item04;

                            SAPProjectTaskExpenseObj.StartDateTime = new SAPProjectMaintainBundleIn.LOCALNORMALISED_DateTime();
                            SAPProjectTaskExpenseObj.StartDateTime.Value = DateTime.Parse(ProjectTaskExpenseItem.PlannedPeriodStartDate.ToString("yyyy-MM-dd") + "T09:00:00Z").ToUniversalTime();
                                

                            SAPProjectTaskExpenseObj.EndDateTime = new SAPProjectMaintainBundleIn.LOCALNORMALISED_DateTime();
                            SAPProjectTaskExpenseObj.EndDateTime.Value = DateTime.Parse(ProjectTaskExpenseItem.PlannedPeriodEndDate.ToString("yyyy-MM-dd") + "T09:00:00Z").ToUniversalTime();                                
                            
                            SAPProjectTaskExpenseObj.Amount = new SAPProjectMaintainBundleIn.Amount();
                            SAPProjectTaskExpenseObj.Amount.Value = ProjectTaskExpenseItem.PalnnedCostAmount;
                            SAPProjectTaskExpenseObj.Amount.currencyCode = ProjectTaskExpenseItem.Currency;

                            if (ProjectTaskExpenseItem.taskExpensePeriodPlan.Count > 0)
                            {
                                int k = 0;
                                //List<SAPProjectMaintainBundleIn.ProjectPeriodPlanMessageBundleRequest> _list = new List<SAPProjectMaintainBundleIn.ProjectPeriodPlanMessageBundleRequest>();
                                SAPProjectTaskExpenseObj.TaskExpensePeriodPlan = new SAPProjectMaintainBundleIn.ProjectPeriodPlanMessageBundleRequest[ProjectTaskExpenseItem.taskExpensePeriodPlan.Count];

                                foreach (TaskExpensePeriodPlan TaskExpensePeriodPlanItem in ProjectTaskExpenseItem.taskExpensePeriodPlan)
                                {
                                    SAPProjectMaintainBundleIn.ProjectPeriodPlanMessageBundleRequest TaskExpensePeriodPlanObj = new SAPProjectMaintainBundleIn.ProjectPeriodPlanMessageBundleRequest();

                                    if (TaskExpensePeriodPlanItem.UUID != null)
                                    {
                                        TaskExpensePeriodPlanObj.UUID = new SAPProjectMaintainBundleIn.UUID();
                                        TaskExpensePeriodPlanObj.UUID.Value = TaskExpensePeriodPlanItem.UUID;
                                        TaskExpensePeriodPlanObj.ActionCode = SAPProjectMaintainBundleIn.ActionCode.Item04;
                                    }
                                    else
                                        TaskExpensePeriodPlanObj.ActionCode = SAPProjectMaintainBundleIn.ActionCode.Item04;

                                    TaskExpensePeriodPlanObj.StartDateTime = new SAPProjectMaintainBundleIn.LOCALNORMALISED_DateTime();
                                    TaskExpensePeriodPlanObj.StartDateTime.Value = DateTime.Parse(TaskExpensePeriodPlanItem.PlannedPeriodStartDate.ToString("yyyy-MM-dd") + "T09:00:00Z").ToUniversalTime(); 
                                        
                                    TaskExpensePeriodPlanObj.EndDateTime = new SAPProjectMaintainBundleIn.LOCALNORMALISED_DateTime();
                                    TaskExpensePeriodPlanObj.EndDateTime.Value = DateTime.Parse(TaskExpensePeriodPlanItem.PlannedPeriodEndDate.ToString("yyyy-MM-dd") + "T09:00:00Z").ToUniversalTime();                                         

                                    TaskExpensePeriodPlanObj.Amount = new SAPProjectMaintainBundleIn.Amount();
                                    TaskExpensePeriodPlanObj.Amount.Value = TaskExpensePeriodPlanItem.PalnnedCostAmount;
                                    TaskExpensePeriodPlanObj.Amount.currencyCode = TaskExpensePeriodPlanItem.CurrencyCode;

                                    TaskExpensePeriodPlanObj.Description = TaskExpensePeriodPlanItem.Description;

                                    SAPProjectTaskExpenseObj.TaskExpensePeriodPlan[k] = new SAPProjectMaintainBundleIn.ProjectPeriodPlanMessageBundleRequest();
                                    SAPProjectTaskExpenseObj.TaskExpensePeriodPlan[k++] = TaskExpensePeriodPlanObj;
                                    //_list.Add(TaskExpensePeriodPlanObj);
                                }

                                //SAPProjectTaskExpenseObj.TaskExpensePeriodPlan = new SAPProjectMaintainBundleIn.ProjectPeriodPlanMessageBundleRequest[_list.Count];
                                //int k = 0;
                                //foreach (SAPProjectMaintainBundleIn.ProjectPeriodPlanMessageBundleRequest item in _list)
                                //{
                                //    SAPProjectTaskExpenseObj.TaskExpensePeriodPlan[k++] = item;
                                //}
                               
                            }
                            SAPProjectTaskRequestObj.ProjectTaskExpense[j] = SAPProjectTaskExpenseObj;
                            j++;
                        }
                    }
                    ProjectObj.ProjectTask[i] = SAPProjectTaskRequestObj;
                    i++;
                }
            }
            request.Project[0] = ProjectObj;
            try
            {
                //Make the call, passing the request
                SAPProjectMaintainBundleIn.ProjectMaintainConfirmationBundleMessage_sync response = svc.MaintainBundle(request);
                return response;
            }
            catch (Exception exp)
            {
                if (exp.Message.ToString().Contains("The remote name could not be resolved") == true)
                {
                    throw new Exception("Network Issue and Can't reach to the SAP ByDesign Server");
                }
                else
                    throw new Exception(exp.Message);
            }
            return null;
        }

    }
}
