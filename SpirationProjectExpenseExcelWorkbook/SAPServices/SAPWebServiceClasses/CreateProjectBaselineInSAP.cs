﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SpirationProjectExpenseExcelWorkbook
{
    public class CreateProjectBaselineInSAP
    {

        public static Boolean CreateProjectBaseline(String ProjectID, String ProjectUUID)
        {
            SAPCreateProjectBaseline.ProjectCreateBaselineCreateBaselineConfirmationMessage_sync response = CallWebService(ProjectID, ProjectUUID);
            if (response != null && response.Log != null && response.Log.MaximumLogItemSeverityCode.Equals("3"))
            {
                StringBuilder SB = new StringBuilder();
                foreach (var item in response.Log.Item)
                {
                    String Str = String.Format(AppConstants.Project_BaseLine_Not_Created + Environment.NewLine + "SeverityCode: {0}, Note Message: {1}{2}", item.SeverityCode, item.Note, Environment.NewLine);
                    SB.Append(Str);
                }
                MessageBox.Show(SB.ToString(), "Error in Creating Project Baseline", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            else if (response != null && response.Log != null)
            {
                StringBuilder SB = new StringBuilder();
                foreach (var item in response.Log.Item)
                {
                    String Str = String.Format("Message: {0}, SeverityCode: {1}{2}", item.Note, item.SeverityCode, Environment.NewLine);
                    SB.Append(Str);
                }

                MessageBox.Show(AppConstants.Project_BaseLine_Created + Environment.NewLine + SB.ToString(), "Project Baseline Created", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return true;
            }
            return false;
        }

        private static SAPCreateProjectBaseline.ProjectCreateBaselineCreateBaselineConfirmationMessage_sync CallWebService(String ProjectID, String ProjectUUID)
        {
            //Create the Web Service reference service object
            SAPCreateProjectBaseline.service svc = new SAPCreateProjectBaseline.service();
            //Set Credentials
            SAPLogInSession _SAPLogInSession = SAPLogInSession.Instance;
            String uName = _SAPLogInSession.Username;
            String uPassword = _SAPLogInSession.Password;

            _SAPLogInSession.Username = AppConstants.InternalSAPUserName;
            _SAPLogInSession.Password = AppConstants.InternalSAPPassword;
            svc.Credentials = new SAPCredentials();


            //Create Request object
            SAPCreateProjectBaseline.ProjectCreateBaselineCreateBaselineRequest_syncMessage request = new SAPCreateProjectBaseline.ProjectCreateBaselineCreateBaselineRequest_syncMessage();

            request.Project = new SAPCreateProjectBaseline.ProjectCreateBaselineCreateBaselineRequest();

            request.Project.ProjectID = new SAPCreateProjectBaseline.ProjectID();
            request.Project.ProjectID.Value = ProjectID;

            request.Project.UUID = new SAPCreateProjectBaseline.UUID();
            request.Project.UUID.Value = ProjectUUID;

            try
            {
                //Make the call, passing the request
                SAPCreateProjectBaseline.ProjectCreateBaselineCreateBaselineConfirmationMessage_sync response = svc.CreateBaseline(request);

                _SAPLogInSession.Username = uName;
                _SAPLogInSession.Password = uPassword;

                return response;
            }
            catch (Exception exp)
            {
                _SAPLogInSession.Username = uName;
                _SAPLogInSession.Password = uPassword;
                if (exp.Message.ToString().Contains("The remote name could not be resolved") == true)
                {
                    throw new Exception("Network Issue and Can't reach to the SAP ByDesign Server");
                }
            }
            return null;
        }

    }
}
