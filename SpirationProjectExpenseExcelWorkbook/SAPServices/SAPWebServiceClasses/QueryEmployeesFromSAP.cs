﻿using SpirationProjectExpenseExcelWorkbook.SAPServices.ObjectManupilationClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpirationProjectExpenseExcelWorkbook.SAPServices.SAPWebServiceClasses
{
    public class QueryEmployeesFromSAP
    {

        public static List<SAPEmployeesData> GetEmployeesData(String EmployeeID)
        {
            SAPQueryEmployeeIn.EmployeeDataSimpleByResponseMessage_sync Response = CallWebService(EmployeeID);

            if (Response == null && Response.EmployeeData == null)
                return null;

            List<SAPEmployeesData> SAPEmployeesDataList = new List<SAPEmployeesData>();

            foreach (SAPQueryEmployeeIn.EmployeeDataResponseEmployee ResponseEmployeeObject in Response.EmployeeData)
            {
                SAPEmployeesData _SAPEmployeesData = new SAPEmployeesData();

                _SAPEmployeesData.EmployeeID = ResponseEmployeeObject.EmployeeID.Value;
                foreach (var item in ResponseEmployeeObject.BiographicalData)
                {
                    if (item.ValidityPeriod != null && item.ValidityPeriod.EndDate.ToString("yyyy").Equals("9999"))
                    {
                        _SAPEmployeesData.GivenName = item.GivenName;
                        _SAPEmployeesData.FamilyName = item.FamilyName;
                    }
                }

                SAPEmployeesDataList.Add(_SAPEmployeesData);
            }
            return SAPEmployeesDataList;
        }

        internal static SAPQueryEmployeeIn.EmployeeDataSimpleByResponseMessage_sync CallWebService(String EmployeeID)
        {
            //Create the Web Service reference service object
            SAPQueryEmployeeIn.service svc = new SAPQueryEmployeeIn.service();
            //Set Credentials
            SAPLogInSession _SAPLogInSession = SAPLogInSession.Instance;
            String uName = _SAPLogInSession.Username;
            String uPassword = _SAPLogInSession.Password;

            _SAPLogInSession.Username = AppConstants.InternalSAPUserName;
            _SAPLogInSession.Password = AppConstants.InternalSAPPassword;

            svc.Credentials = new SAPCredentials();

            // Set the URL to the Specified URL in the Application Prefrences Setting Object
            //svc.Url = svc.Url.Replace("my335110", "?????");

            //Create Request object
            SAPQueryEmployeeIn.EmployeeDataSimpleByQueryMessage_sync request = new SAPQueryEmployeeIn.EmployeeDataSimpleByQueryMessage_sync();

            //Initialise the instance of 'CustomerSelectionByElements' object on the request
            request.EmployeeDataSelectionByIdentification = new SAPQueryEmployeeIn.EmployeeDataSelectionByIdentification();

            SAPQueryEmployeeIn.EmployeeSelectionByEmployeeID[] Param = new SAPQueryEmployeeIn.EmployeeSelectionByEmployeeID[1];

            Param[0] = new SAPQueryEmployeeIn.EmployeeSelectionByEmployeeID();

            //Include
            Param[0].InclusionExclusionCode = "I";
            //Equals
            Param[0].IntervalBoundaryTypeCode = "1";

            Param[0].LowerBoundaryEmployeeID = new SAPQueryEmployeeIn.EmployeeID();
            Param[0].LowerBoundaryEmployeeID.Value = EmployeeID;

            //Initialise the instance with your Parameters
            request.EmployeeDataSelectionByIdentification.SelectionByEmployeeID = Param;

            //Initialise the instance of 'Processing Conditions' object on the request
            request.PROCESSING_CONDITIONS = new SAPQueryEmployeeIn.QueryProcessingConditions();

            //set processing conditions
            request.PROCESSING_CONDITIONS.QueryHitsMaximumNumberValueSpecified = false;
            request.PROCESSING_CONDITIONS.QueryHitsUnlimitedIndicator = true;

            try
            {
                //Make the call, passing the request
                SAPQueryEmployeeIn.EmployeeDataSimpleByResponseMessage_sync response = svc.FindByIdentification(request);
                _SAPLogInSession.Username = uName;
                _SAPLogInSession.Password = uPassword;

                return response;
            }
            catch (Exception exp)
            {
                _SAPLogInSession.Username = uName;
                _SAPLogInSession.Password = uPassword;
                if (exp.Message.ToString().Contains("The remote name could not be resolved") == true)
                {
                    throw new Exception("Network Issue and Can't reach to the SAP ByDesign Server");
                }
            }
            return null;
        }



    }
}
