using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace SpirationProjectExpenseExcelWorkbook
{
    public class SAPCredentials : ICredentials
    {
        public NetworkCredential GetCredential(Uri uri, string authType)
        {
            SAPLogInSession _SAPLogInSession = SAPLogInSession.Instance;
            return new NetworkCredential(_SAPLogInSession.Username, _SAPLogInSession.Password);
        }
    }
}
