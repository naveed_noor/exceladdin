using System;
using System.Collections.Generic;
using System.Linq;

namespace SpirationProjectExpenseExcelWorkbook
{
    public class TaskExpensePeriodPlan
    {
        public DateTime PlannedPeriodStartDate { get; set; }
        public DateTime PlannedPeriodEndDate { get; set; }
        public decimal PalnnedCostAmount { get; set; }
        public String UUID { get; set; }
        public Boolean InternalIndicator { get; set; }
        public String CurrencyCode { get; set; }

        private String _CurrencyLookup;
        public String CurrencyLookup
        {
            get { return _CurrencyLookup; }
            set
            {
                _CurrencyLookup = value;

                if (_CurrencyLookup != null && !_CurrencyLookup.Trim().Equals(""))
                {
                    string val = _CurrencyLookup.Substring(0, _CurrencyLookup.IndexOf(" - ")).Trim();
                    if (!val.Equals(CurrencyCode))
                        CurrencyCode = val;
                }
            }
        }
        public String Description { get; set; }
    }
}
