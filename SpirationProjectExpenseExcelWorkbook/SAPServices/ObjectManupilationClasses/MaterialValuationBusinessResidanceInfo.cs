﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpirationProjectExpenseExcelWorkbook
{
    public class MaterialValuationBusinessResidanceInfo
    {
        private String _MaterialID;
        public String MaterialID
        {
            get { return _MaterialID; }
            set { _MaterialID = value; }
        }
        
        private String _BusinessResidenceID;
        public String BusinessResidenceID
        {
            get { return _BusinessResidenceID; }
            set { _BusinessResidenceID = value; }
        }
        
        private String _CompanyID;
        public String CompanyID
        {
            get { return _CompanyID; }
            set { _CompanyID = value; }
        }
    }
}
