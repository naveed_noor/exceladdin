﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpirationProjectExpenseExcelWorkbook.SAPServices.ObjectManupilationClasses
{
    [Serializable()]
    public class SAPEmployeesData
    {
        public String EmployeeID;
        public String FamilyName;
        public String GivenName;
    }
}
