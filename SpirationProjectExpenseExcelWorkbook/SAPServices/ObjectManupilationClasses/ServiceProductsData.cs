﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpirationProjectExpenseExcelWorkbook.SAPServices.ObjectManupilationClasses
{
    [Serializable()]
    public class ServiceProductsData
    {
        public String InternalID;
        public String Description;
    }
}
