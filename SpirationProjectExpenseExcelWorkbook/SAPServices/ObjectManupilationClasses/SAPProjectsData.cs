﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpirationProjectExpenseExcelWorkbook
{
    public class SAPProjectsData
    {

        public String ProjectID { get; set; }
        
        public String ProjectUUID { get; set; }

        public List<ProjectTask> ProjectTask { get; set; }
        public SAPProjectsData()
        {
            ProjectTask = new List<ProjectTask>();
        }
    }
}