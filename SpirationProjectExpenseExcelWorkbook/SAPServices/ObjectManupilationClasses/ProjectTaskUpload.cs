﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpirationProjectExpenseExcelWorkbook.SAPServices.ObjectManupilationClasses
{
    public class ProjectTaskUpload
    {

        public ProjectTaskUpload()
        {
            ProductTypeCode = 2;
            ProductIdentifierTypeCode = 1;
            TimeZoneCode = "UTC";
        }

        private String _ProjectID;
        public String ProjectID
        {
            get { return _ProjectID; }
            set { _ProjectID = value; }
        }

        private String _ProjectTaskID;
        public String ProjectTaskID
        {
            get { return _ProjectTaskID; }
            set { _ProjectTaskID = value; }
        }

        private String _AssignedEmployeeID;
        public String AssignedEmployeeID
        {
            get { return _AssignedEmployeeID; }
            set { _AssignedEmployeeID = value; }
        }

        private double _PlannedWorkQuantity;
        public double PlannedWorkQuantity
        {
            get { return _PlannedWorkQuantity; }
            set { _PlannedWorkQuantity = value; }
        }

        private String _ProductID;
        public String ProductID
        {
            get { return _ProductID; }
            set { _ProductID = value; }
        }

        private int _ProductTypeCode;
        public int ProductTypeCode
        {
            get { return _ProductTypeCode; }
            set { _ProductTypeCode = value; }
        }
        
        private int _ProductIdentifierTypeCode;
        public int ProductIdentifierTypeCode
        {
            get { return _ProductIdentifierTypeCode; }
            set { _ProductIdentifierTypeCode = value; }
        }

        private String _timeZoneCode;
        public String TimeZoneCode
        {
            get { return _timeZoneCode; }
            set { _timeZoneCode = value; }
        }

        private DateTime _PlannedPeriodStartDate;
        public DateTime PlannedPeriodStartDate
        {
            get { return _PlannedPeriodStartDate; }
            set { _PlannedPeriodStartDate = value; }
        }

        private DateTime _PlannedPeriodEndDate;
        public DateTime PlannedPeriodEndDate
        {
            get { return _PlannedPeriodEndDate; }
            set { _PlannedPeriodEndDate = value; }
        }

    }
}
