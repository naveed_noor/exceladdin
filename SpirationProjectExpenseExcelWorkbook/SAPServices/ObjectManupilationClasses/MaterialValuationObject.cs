using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;

namespace SpirationProjectExpenseExcelWorkbook
{
    public class MaterialValuationObject : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        // This method is called by the Set accessor of each property.
        // The CallerMemberName attribute that is applied to the optional propertyName
        // parameter causes the property name of the caller to be substituted as an argument.
        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private String _CompanyID;
        public String CompanyID
        {
            get { return _CompanyID; }
            set { _CompanyID = value; }
        }

        private String _SetOfBooksID;
        public String SetOfBooksID
        {
            get { return _SetOfBooksID; }
            set { _SetOfBooksID = value; }
        }

        private String _BusinessResidenceID;
        public String BusinessResidenceID
        {
            get { return _BusinessResidenceID; }
            set {
                if (value != _BusinessResidenceID)
                {
                    _BusinessResidenceID = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private String _MaterialID;
        public String MaterialID
        {
            get { return _MaterialID; }
            set { _MaterialID = value; }
        }

        private String _ValuationLevelID;
        public String ValuationLevelID
        {
            get { return _ValuationLevelID; }
            set { _ValuationLevelID = value; }
        }

        private String _CostType;
        public String CostType
        {
            get { return _CostType; }
            set { _CostType = value; }
        }

        private double _Cost;
        public double Cost
        {
            get { return _Cost; }
            set { _Cost = value; }
        }

        private String _Currency;
        public String Currency
        {
            get { return _Currency; }
            set { _Currency = value; }
        }

        private String _CostUnit;
        public String CostUnit
        {
            get { return _CostUnit; }
            set { _CostUnit = value; }
        }

        private String _CostUoM;
        public String CostUoM
        {
            get { return _CostUoM; }
            set { _CostUoM = value; }
        }

        private String _AccountDeterminationGroup;
        public String AccountDeterminationGroup
        {
            get { return _AccountDeterminationGroup; }
            set { _AccountDeterminationGroup = value; }
        }

        private DateTime _ValidFrom;
        public DateTime ValidFrom
        {
            get { return _ValidFrom; }
            set { _ValidFrom = value; }
        }

        private DateTime _ValidTo;
        public DateTime ValidTo
        {
            get { return _ValidTo; }
            set { _ValidTo = value; }
        }

        private Boolean _IsBusinessResidanceCreatedInSAP;
        public Boolean IsBusinessResidanceCreatedInSAP
        {
            get { return _IsBusinessResidanceCreatedInSAP; }
            set { _IsBusinessResidanceCreatedInSAP = value; }
        }

        private String _BusinessResidenceSAPMessage;
        public String BusinessResidenceSAPMessage
        {
            get { return _BusinessResidenceSAPMessage; }
            set { _BusinessResidenceSAPMessage = value; }
        }

        private Boolean _IsPushRequestSentToSAP;
        public Boolean IsPushRequestSentToSAP
        {
            get { return _IsPushRequestSentToSAP; }
            set { _IsPushRequestSentToSAP = value; }
        }

        private Boolean _IsPushedInSAP;
        public Boolean IsPushedInSAP
        {
            get { return _IsPushedInSAP; }
            set { _IsPushedInSAP = value; }
        }

        private DateTime _PushDateTime;
        public DateTime PushDateTime
        {
            get { return _PushDateTime; }
            set { _PushDateTime = value; }
        }

        private String _PushStatusMessageFromSAP;
        public String PushStatusMessageFromSAP
        {
            get { return _PushStatusMessageFromSAP; }
            set { _PushStatusMessageFromSAP = value; }
        }

        private Boolean _IsValuationExistsInSAP;
        public Boolean IsValuationExistsInSAP
        {
            get { return _IsValuationExistsInSAP; }
            set { _IsValuationExistsInSAP = value; }
        }
    }
}
