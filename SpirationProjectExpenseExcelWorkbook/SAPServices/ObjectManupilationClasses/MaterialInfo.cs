using System;
using System.Collections.Generic;
using System.Linq;

namespace SpirationProjectExpenseExcelWorkbook
{
    [Serializable]
    public class MaterialInfo
    {
        private String _MaterialID;

        public String MaterialID
        {
            get { return _MaterialID; }
            set { _MaterialID = value; }
        }
        private String _MaterialUUID;

        public String MaterialUUID
        {
            get { return _MaterialUUID; }
            set { _MaterialUUID = value; }
        }
        private String _BaseMeasureUnitCode;

        public String BaseMeasureUnitCode
        {
            get { return _BaseMeasureUnitCode; }
            set { _BaseMeasureUnitCode = value; }
        }
        private String _InventoryValuationMeasureUnitCode;

        public String InventoryValuationMeasureUnitCode
        {
            get { return _InventoryValuationMeasureUnitCode; }
            set { _InventoryValuationMeasureUnitCode = value; }
        }
        private String _Description;

        public String Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        public List<ValuationInfo> ValuationInfoList = new List<ValuationInfo>();
    }

     [Serializable]
    public class ValuationInfo
    {
        private Int32 lifeCycleStatusCode;
        public Int32 LifeCycleStatusCode
        {
            get { return lifeCycleStatusCode; }
            set { lifeCycleStatusCode = value; }
        }

        private String companyID;
        public String CompanyID
        {
            get { return companyID; }
            set { companyID = value; }
        }


        private String businessResidenceID;
        public String BusinessResidenceID
        {
            get { return businessResidenceID; }
            set { businessResidenceID = value; }
        }
    }
}
