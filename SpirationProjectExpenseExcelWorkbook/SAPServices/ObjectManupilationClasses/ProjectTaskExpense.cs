using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;

namespace SpirationProjectExpenseExcelWorkbook
{
    public class ProjectTaskExpense //: INotifyPropertyChanged
    {

        //public event PropertyChangedEventHandler PropertyChanged;
        //private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        //{
        //    if (PropertyChanged != null)
        //    {
        //        PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        //    }
        //}

        private String fTaskExpenseDescription;
        public String TaskExpenseDescription { 
            get 
            { 
                return this.fTaskExpenseDescription; 
            }

            set 
            { 
                this.fTaskExpenseDescription = value; 
                //NotifyPropertyChanged(); 
            }
        }

        public decimal PalnnedCostAmount { get; set; }

        private String _CurrencyLookup;
        public String CurrencyLookup
        {
            get { return _CurrencyLookup; }
            set
            {
                _CurrencyLookup = value;

                if (_CurrencyLookup != null && !_CurrencyLookup.Trim().Equals(""))
                {
                    string val = _CurrencyLookup.Substring(0, _CurrencyLookup.IndexOf(" - ")).Trim();
                    if (!val.Equals(Currency))
                        Currency = val;
                }
            }
        }
        public String Currency { get; set; }


        private String _GeneralLedgerAccountAliasCodeLookup;
        public String GeneralLedgerAccountAliasCodeLookup
        {
            get { return _GeneralLedgerAccountAliasCodeLookup; }
            set
            {
                _GeneralLedgerAccountAliasCodeLookup = value;
                if (_GeneralLedgerAccountAliasCodeLookup != null && !_GeneralLedgerAccountAliasCodeLookup.Trim().Equals(""))
                {
                    string val = _GeneralLedgerAccountAliasCodeLookup.Substring(0, _GeneralLedgerAccountAliasCodeLookup.IndexOf(" - ")).Trim();
                    if (!val.Equals(GeneralLedgerAccountAliasCode))
                        GeneralLedgerAccountAliasCode = val;
                }
            }
        }
        public String GeneralLedgerAccountAliasCode { get; set; }

        public String ProjectTaskExpenseUUID { get; set; }

        public DateTime PlannedPeriodStartDate { get; set; }
        public DateTime PlannedPeriodEndDate { get; set; }

        public List<TaskExpensePeriodPlan> taskExpensePeriodPlan { get; set; }

        public ProjectTaskExpense()
        {
            taskExpensePeriodPlan = new List<TaskExpensePeriodPlan>();
        }
    }
}
