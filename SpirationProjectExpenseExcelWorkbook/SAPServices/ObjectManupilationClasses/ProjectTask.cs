using System;
using System.Collections.Generic;
using System.Linq;

namespace SpirationProjectExpenseExcelWorkbook
{
    public class ProjectTask
    {
        public String TaskName { get; set; }

        public DateTime EarliestStartDate { get; set; }
        public DateTime LatestEndDate { get; set; }
        public String ProjectElementID { get; set; }
        public String ProjectTaskUUID { get; set; }

        public int ProjectTaskExpenseCount;
        public List<ProjectTaskExpense> ProjectTaskExpense { get; set; }

        public ProjectTask()
        {
            ProjectTaskExpense = new List<ProjectTaskExpense>();
        }
    }
}
