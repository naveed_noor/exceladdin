﻿using SpirationProjectExpenseExcelWorkbook.SAPServices.SAPWebServiceClasses;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpirationProjectExpenseExcelWorkbook.MaterialValuationUpload
{
    public class ManageMaterialLookup
    {

        public void QueryFromSAPAndSaveMaterials()
        {
            ManageMaterialValuation _ManageMaterialValuation = new ManageMaterialValuation();
            List<MaterialInfo> MaterialInfoList = _ManageMaterialValuation.QueryMaterialInfo("*");

            string path = Path.GetTempPath();
            String FilePath = string.Format("{0}\\MaterialInfo.txt", path);
            WriteFile(MaterialInfoList, FilePath);
        }

        public List<MaterialInfo> GetMaterialInfo()
        {
            string path = Path.GetTempPath();
            String FilePath = string.Format("{0}\\MaterialInfo.txt", path);

            List<MaterialInfo> MaterialInfoList  = ReadFile(FilePath);
            if (MaterialInfoList == null)
            {
                QueryFromSAPAndSaveMaterials();
                MaterialInfoList = ReadFile(FilePath);
            }
            return MaterialInfoList;
        }

        private void WriteFile(List<MaterialInfo> CodesList, String FileName)
        {
            try
            {
                File.Delete(FileName);
            }
            catch (Exception exp)
            {
            }

            using (Stream stream = File.Open(FileName, FileMode.Create))
            {
                var bformatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

                bformatter.Serialize(stream, CodesList);
            }
        }

        private List<MaterialInfo> ReadFile(String FileName)
        {
            try
            {
                using (Stream stream = File.Open(FileName, FileMode.Open))
                {
                    var bformatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                    List<MaterialInfo> CodesList = (List<MaterialInfo>)bformatter.Deserialize(stream);
                    return CodesList;
                }
            }
            catch (Exception FNFexp)
            {
                return null;
            }
        }

    }
}
