﻿using SpirationProjectExpenseExcelWorkbook.MaterialValuationUpload;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using Office = Microsoft.Office.Core;

namespace SpirationProjectExpenseExcelWorkbook.CustomControls
{
    partial class MaterialSelectorPaneControl : UserControl
    {
        public MaterialSelectorPaneControl()
        {
            InitializeComponent();
        }

        private void MaterialSelectorPaneControl_Load(object sender, EventArgs e)
        {
            ManageMaterialLookup _ManageMaterialLookup = new ManageMaterialLookup();
            List<MaterialInfo> MaterialInfoList = _ManageMaterialLookup.GetMaterialInfo();
            List<String> materialList = new List<string>();

            foreach (var item in MaterialInfoList)
            {
                materialList.Add(item.MaterialID + " - " + item.Description);
            }
            materialList.Sort();
            ((ListBox)this.MaterialsCheckedListBox).DataSource = materialList;

            List<BusinessResidence> BusinessResidenceList = Globals.MaterialValuationUploadSheet.GetBusinessResidenceList();
            List<String> businessResidenceStringList = new List<string>();
            foreach (var item in BusinessResidenceList)
            {
                businessResidenceStringList.Add(item.BusinessResidenceID + " - " + item.BusinessResidenceName);
            }
            ((ListBox)this.BusinessResidancesCheckedListBox).DataSource = businessResidenceStringList;
        }

        private void CreateEntriesBtn_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            List<String> materialList = new List<string>();
            foreach (var item in this.MaterialsCheckedListBox.CheckedItems)
            {
                String Str = (String)item;
                String MaterialIDStr = GetFirstPart(Str);
                materialList.Add(MaterialIDStr);                
            }

            List<String> businessResidenceStringList = new List<string>();
            foreach (var item in this.BusinessResidancesCheckedListBox.CheckedItems)
            {
                String Str = (String)item;
                String BusinessResidanceIDStr = GetFirstPart(Str);
                businessResidenceStringList.Add(BusinessResidanceIDStr);
            }
            Globals.MaterialValuationUploadSheet.CreateMaterialRowsForAllBusinessResidences(materialList, businessResidenceStringList);

            for (int i = 0; i < MaterialsCheckedListBox.Items.Count; i++)
                this.MaterialsCheckedListBox.SetItemCheckState(i, CheckState.Unchecked);

            for (int i = 0; i < BusinessResidancesCheckedListBox.Items.Count; i++)
                this.BusinessResidancesCheckedListBox.SetItemCheckState(i, CheckState.Unchecked);
            
            this.Hide();
            Globals.ThisWorkbook.Application.DisplayDocumentActionTaskPane = false;
            Cursor.Current = Cursors.Default;
        }

        private String GetFirstPart(String Value)
        {
            String str = Value.Substring(0, Value.IndexOf(" - ")).Trim();
            return str;
        }
    }
}
