﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using Office = Microsoft.Office.Core;

namespace SpirationProjectExpenseExcelWorkbook
{
    partial class QueryProjectsActionPaneControl : UserControl
    {
        public QueryProjectsActionPaneControl()
        {
            InitializeComponent();
        }

        private void QueryProjectsFromSAPBtn_Click(object sender, EventArgs e)
        {
            if (ProjectIDTxt.Text.Trim().Equals(""))
            {
                MessageBox.Show(AppConstants.Enter_Project_ID, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            else if (ProjectIDTxt.Text.Trim().Equals("*"))
            {
                var res = MessageBox.Show(AppConstants.Load_All_Project_Confirmation, "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                if (res == DialogResult.No)
                    return;
            }
            Cursor.Current = Cursors.WaitCursor;
            List<SAPProjectsData> _SAPProjectsData = QueryProjectFromSAP.GetProjectData(ProjectIDTxt.Text);
            Cursor.Current = Cursors.Default;
            if (_SAPProjectsData == null)
                MessageBox.Show(AppConstants.No_Project_Found_In_SAP, "No Project Found", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            else
                Globals.Sheet1.SetSheetProjectsData(_SAPProjectsData);
        }
    }
}
