﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using Office = Microsoft.Office.Core;

namespace SpirationProjectExpenseExcelWorkbook
{
    partial class CreateProjectBaselineActionsPaneControl : UserControl
    {
        public CreateProjectBaselineActionsPaneControl()
        {
            InitializeComponent();
        }

        private void QueryProjectsFromSAPBtn_Click(object sender, EventArgs e)
        {
            if (ProjectIDTxt.Text.Trim().Equals(""))
            {
                MessageBox.Show(AppConstants.Enter_Exact_Project_ID, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            else if (ProjectIDTxt.Text.Trim().Equals("*"))
            {
                var res = MessageBox.Show(AppConstants.All_Projects_Loading_Not_Allowed, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            List<SAPProjectsData> _SAPProjectsData = QueryProjectFromSAP.GetProjectData(ProjectIDTxt.Text);
            Cursor.Current = Cursors.Default;

            if (_SAPProjectsData == null)
                MessageBox.Show(AppConstants.No_Project_Found_In_SAP, "No Data Found", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            else if (_SAPProjectsData.Count > 1)
            {
                MessageBox.Show(AppConstants.More_Then_One_Project_Returned, AppConstants.Multiple_Project_Message_Title, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (_SAPProjectsData.Count == 1)
            {
                this.ProjectIDResTxt.Text = _SAPProjectsData[0].ProjectID;
                this.ProjectUUIDResTxt.Text = _SAPProjectsData[0].ProjectUUID;
            }
        }

        private void CreateBaselineInSAPBtn_Click(object sender, EventArgs e)
        {
            if (this.ProjectIDResTxt.Text.Equals("") || this.ProjectUUIDResTxt.Text.Equals(""))
            {
                MessageBox.Show(AppConstants.First_Load_Project);
                return;
            }
            else if (!this.ProjectIDResTxt.Text.Equals("") && !this.ProjectUUIDResTxt.Text.Equals(""))
            {
                Cursor.Current = Cursors.WaitCursor;
                CreateProjectBaselineInSAP.CreateProjectBaseline(this.ProjectIDResTxt.Text, this.ProjectUUIDResTxt.Text);
                Cursor.Current = Cursors.Default;
            }
        }
    }
}
