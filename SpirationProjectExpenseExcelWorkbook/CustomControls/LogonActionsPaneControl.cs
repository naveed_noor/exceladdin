﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using Office = Microsoft.Office.Core;

namespace SpirationProjectExpenseExcelWorkbook
{
    partial class LogonActionsPaneControl : UserControl
    {
        public LogonActionsPaneControl()
        {
            InitializeComponent();
           
        }

        private void SAPLogonBtn_Click_1(object sender, EventArgs e)
        {
            SAPLogInSession _SAPLogInSession = SAPLogInSession.Instance;

            if (_SAPLogInSession.IsLoggedIntoSAP == true)
            {
                MessageBox.Show(AppConstants.Already_Logged_In_SAP, AppConstants.Already_Logged_In_SAP_Title, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (SAPTanentIDTxt.Text.Equals(""))
            {
                MessageBox.Show(AppConstants.Enter_SAP_Tenant_ID, "Tanent ID Required", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            if (SAPUserNameTxt.Text.Equals(""))
            {
                MessageBox.Show(AppConstants.Enter_User_Name, "Username Required", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            if (SAPPasswordTxt.Text.Equals(""))
            {
                MessageBox.Show(AppConstants.Enter_Password, "Password Required", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            Cursor.Current = Cursors.WaitCursor;

            _SAPLogInSession.TanentID = SAPTanentIDTxt.Text;
            _SAPLogInSession.Username = SAPUserNameTxt.Text;
            _SAPLogInSession.Password = SAPPasswordTxt.Text;

            Boolean result = CallLogonToSAPWebServices.LogonToSAP(SAPTanentIDTxt.Text, SAPUserNameTxt.Text, SAPPasswordTxt.Text);

            Cursor.Current = Cursors.Default;

            if (result == true)
            {
                MessageBox.Show(AppConstants.Logged_In_SAP, "Logon sucessfully", MessageBoxButtons.OK, MessageBoxIcon.Information);
                _SAPLogInSession.IsLoggedIntoSAP = true;
                this.Hide();
                Globals.ThisWorkbook.Application.DisplayDocumentActionTaskPane = false;
                SAPLogonBtn.Text = "Loged In";
            }
            else
            {
                MessageBox.Show(AppConstants.Invalid_SAP_Credentials_Entered, "Logon Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _SAPLogInSession.IsLoggedIntoSAP = false;
                SAPLogonBtn.Text = "Logon";
            }
        }

        private void LogonActionsPaneControl_Load(object sender, EventArgs e)
        {
            SAPLogInSession _SAPLogInSession = SAPLogInSession.Instance;
            this.SAPTanentIDTxt.Text = _SAPLogInSession.TanentID;
        }
    }
}
