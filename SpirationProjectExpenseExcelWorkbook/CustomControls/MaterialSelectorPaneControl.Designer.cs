﻿namespace SpirationProjectExpenseExcelWorkbook.CustomControls
{
    [System.ComponentModel.ToolboxItemAttribute(false)]
    partial class MaterialSelectorPaneControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MaterialsCheckedListBox = new System.Windows.Forms.CheckedListBox();
            this.CreateEntriesBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.BusinessResidancesCheckedListBox = new System.Windows.Forms.CheckedListBox();
            this.SuspendLayout();
            // 
            // MaterialsCheckedListBox
            // 
            this.MaterialsCheckedListBox.FormattingEnabled = true;
            this.MaterialsCheckedListBox.Location = new System.Drawing.Point(2, 29);
            this.MaterialsCheckedListBox.Name = "MaterialsCheckedListBox";
            this.MaterialsCheckedListBox.Size = new System.Drawing.Size(402, 544);
            this.MaterialsCheckedListBox.TabIndex = 0;
            // 
            // CreateEntriesBtn
            // 
            this.CreateEntriesBtn.Location = new System.Drawing.Point(88, 681);
            this.CreateEntriesBtn.Name = "CreateEntriesBtn";
            this.CreateEntriesBtn.Size = new System.Drawing.Size(163, 23);
            this.CreateEntriesBtn.TabIndex = 1;
            this.CreateEntriesBtn.Text = "Create Entries";
            this.CreateEntriesBtn.UseVisualStyleBackColor = true;
            this.CreateEntriesBtn.Click += new System.EventHandler(this.CreateEntriesBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(156, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Select Material to create entries";
            // 
            // BusinessResidancesCheckedListBox
            // 
            this.BusinessResidancesCheckedListBox.FormattingEnabled = true;
            this.BusinessResidancesCheckedListBox.Location = new System.Drawing.Point(4, 579);
            this.BusinessResidancesCheckedListBox.Name = "BusinessResidancesCheckedListBox";
            this.BusinessResidancesCheckedListBox.Size = new System.Drawing.Size(400, 94);
            this.BusinessResidancesCheckedListBox.TabIndex = 3;
            // 
            // MaterialSelectorPaneControl
            // 
            this.Controls.Add(this.BusinessResidancesCheckedListBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.CreateEntriesBtn);
            this.Controls.Add(this.MaterialsCheckedListBox);
            this.Name = "MaterialSelectorPaneControl";
            this.Size = new System.Drawing.Size(407, 719);
            this.Load += new System.EventHandler(this.MaterialSelectorPaneControl_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckedListBox MaterialsCheckedListBox;
        private System.Windows.Forms.Button CreateEntriesBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckedListBox BusinessResidancesCheckedListBox;
    }
}
