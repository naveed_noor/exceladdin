﻿namespace SpirationProjectExpenseExcelWorkbook
{
    [System.ComponentModel.ToolboxItemAttribute(false)]
    partial class CreateProjectBaselineActionsPaneControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.QueryProjectsFromSAPBtn = new System.Windows.Forms.Button();
            this.ProjectIDTxt = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.CreateBaselineInSAPBtn = new System.Windows.Forms.Button();
            this.ProjectIDResTxt = new System.Windows.Forms.TextBox();
            this.ProjectUUIDResTxt = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // QueryProjectsFromSAPBtn
            // 
            this.QueryProjectsFromSAPBtn.Location = new System.Drawing.Point(14, 53);
            this.QueryProjectsFromSAPBtn.Name = "QueryProjectsFromSAPBtn";
            this.QueryProjectsFromSAPBtn.Size = new System.Drawing.Size(219, 23);
            this.QueryProjectsFromSAPBtn.TabIndex = 5;
            this.QueryProjectsFromSAPBtn.Text = "Query SAP Project";
            this.QueryProjectsFromSAPBtn.UseVisualStyleBackColor = true;
            this.QueryProjectsFromSAPBtn.Click += new System.EventHandler(this.QueryProjectsFromSAPBtn_Click);
            // 
            // ProjectIDTxt
            // 
            this.ProjectIDTxt.Location = new System.Drawing.Point(14, 27);
            this.ProjectIDTxt.Name = "ProjectIDTxt";
            this.ProjectIDTxt.Size = new System.Drawing.Size(219, 20);
            this.ProjectIDTxt.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Project ID";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 103);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Project ID:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 158);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Project UUID:";
            // 
            // CreateBaselineInSAPBtn
            // 
            this.CreateBaselineInSAPBtn.Location = new System.Drawing.Point(14, 213);
            this.CreateBaselineInSAPBtn.Name = "CreateBaselineInSAPBtn";
            this.CreateBaselineInSAPBtn.Size = new System.Drawing.Size(219, 23);
            this.CreateBaselineInSAPBtn.TabIndex = 8;
            this.CreateBaselineInSAPBtn.Text = "Create Baseline in SAP";
            this.CreateBaselineInSAPBtn.UseVisualStyleBackColor = true;
            this.CreateBaselineInSAPBtn.Click += new System.EventHandler(this.CreateBaselineInSAPBtn_Click);
            // 
            // ProjectIDResTxt
            // 
            this.ProjectIDResTxt.Enabled = false;
            this.ProjectIDResTxt.Location = new System.Drawing.Point(14, 119);
            this.ProjectIDResTxt.Name = "ProjectIDResTxt";
            this.ProjectIDResTxt.Size = new System.Drawing.Size(219, 20);
            this.ProjectIDResTxt.TabIndex = 9;
            // 
            // ProjectUUIDResTxt
            // 
            this.ProjectUUIDResTxt.Enabled = false;
            this.ProjectUUIDResTxt.Location = new System.Drawing.Point(14, 174);
            this.ProjectUUIDResTxt.Name = "ProjectUUIDResTxt";
            this.ProjectUUIDResTxt.Size = new System.Drawing.Size(219, 20);
            this.ProjectUUIDResTxt.TabIndex = 10;
            // 
            // CreateProjectBaselineActionsPaneControl
            // 
            this.Controls.Add(this.ProjectUUIDResTxt);
            this.Controls.Add(this.ProjectIDResTxt);
            this.Controls.Add(this.CreateBaselineInSAPBtn);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.QueryProjectsFromSAPBtn);
            this.Controls.Add(this.ProjectIDTxt);
            this.Controls.Add(this.label1);
            this.Name = "CreateProjectBaselineActionsPaneControl";
            this.Size = new System.Drawing.Size(250, 256);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button QueryProjectsFromSAPBtn;
        private System.Windows.Forms.TextBox ProjectIDTxt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button CreateBaselineInSAPBtn;
        private System.Windows.Forms.TextBox ProjectIDResTxt;
        private System.Windows.Forms.TextBox ProjectUUIDResTxt;
    }
}
