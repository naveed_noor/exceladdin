﻿namespace SpirationProjectExpenseExcelWorkbook
{
    [System.ComponentModel.ToolboxItemAttribute(false)]
    partial class LogonActionsPaneControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.SAPTanentIDTxt = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.SAPUserNameTxt = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.SAPPasswordTxt = new System.Windows.Forms.TextBox();
            this.SAPLogonBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(2, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "SAP Tanent ID";
            // 
            // SAPTanentIDTxt
            // 
            this.SAPTanentIDTxt.Location = new System.Drawing.Point(2, 31);
            this.SAPTanentIDTxt.Name = "SAPTanentIDTxt";
            this.SAPTanentIDTxt.Size = new System.Drawing.Size(144, 20);
            this.SAPTanentIDTxt.TabIndex = 9;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(2, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "SAP User Name";
            // 
            // SAPUserNameTxt
            // 
            this.SAPUserNameTxt.Location = new System.Drawing.Point(2, 79);
            this.SAPUserNameTxt.Name = "SAPUserNameTxt";
            this.SAPUserNameTxt.Size = new System.Drawing.Size(144, 20);
            this.SAPUserNameTxt.TabIndex = 11;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(2, 107);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "SAP Password";
            // 
            // SAPPasswordTxt
            // 
            this.SAPPasswordTxt.Location = new System.Drawing.Point(2, 128);
            this.SAPPasswordTxt.Name = "SAPPasswordTxt";
            this.SAPPasswordTxt.PasswordChar = '*';
            this.SAPPasswordTxt.Size = new System.Drawing.Size(144, 20);
            this.SAPPasswordTxt.TabIndex = 13;
            // 
            // SAPLogonBtn
            // 
            this.SAPLogonBtn.Image = global::SpirationProjectExpenseExcelWorkbook.Properties.Resources.SAP_Logon;
            this.SAPLogonBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.SAPLogonBtn.Location = new System.Drawing.Point(2, 157);
            this.SAPLogonBtn.Name = "SAPLogonBtn";
            this.SAPLogonBtn.Size = new System.Drawing.Size(144, 52);
            this.SAPLogonBtn.TabIndex = 14;
            this.SAPLogonBtn.Text = "                 Logon";
            this.SAPLogonBtn.UseVisualStyleBackColor = true;
            this.SAPLogonBtn.Click += new System.EventHandler(this.SAPLogonBtn_Click_1);
            // 
            // LogonActionsPaneControl
            // 
            this.Controls.Add(this.label1);
            this.Controls.Add(this.SAPTanentIDTxt);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.SAPUserNameTxt);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.SAPPasswordTxt);
            this.Controls.Add(this.SAPLogonBtn);
            this.Name = "LogonActionsPaneControl";
            this.Size = new System.Drawing.Size(150, 259);
            this.Load += new System.EventHandler(this.LogonActionsPaneControl_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox SAPTanentIDTxt;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox SAPUserNameTxt;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox SAPPasswordTxt;
        private System.Windows.Forms.Button SAPLogonBtn;

    }
}
