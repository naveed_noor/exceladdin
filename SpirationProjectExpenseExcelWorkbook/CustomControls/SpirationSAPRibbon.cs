﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Office.Tools.Ribbon;
using System.Windows.Forms;
using SpirationProjectExpenseExcelWorkbook.SAPServices.SAPServicesManagementClasses;
using System.IO;
using SpirationProjectExpenseExcelWorkbook.MaterialValuationUpload;
using SpirationProjectExpenseExcelWorkbook.CustomControls;


namespace SpirationProjectExpenseExcelWorkbook
{
    public partial class SpirationSAPRibbon
    {
        private LogonActionsPaneControl _LogonActionsPaneControl = new LogonActionsPaneControl();
        private QueryProjectsActionPaneControl _QueryProjectsActionPaneControl = new QueryProjectsActionPaneControl();
        private CreateProjectBaselineActionsPaneControl _CreateProjectBaselineActionsPaneControl = new CreateProjectBaselineActionsPaneControl();
        private MaterialSelectorPaneControl _MaterialSelectorPaneControl = new MaterialSelectorPaneControl();

        private void SpirationSAPRibbon_Load(object sender, RibbonUIEventArgs e)
        {
            Globals.ThisWorkbook.ActionsPane.Controls.Add(_LogonActionsPaneControl);
            Globals.ThisWorkbook.ActionsPane.Controls.Add(_QueryProjectsActionPaneControl);
            Globals.ThisWorkbook.ActionsPane.Controls.Add(_CreateProjectBaselineActionsPaneControl);
            Globals.ThisWorkbook.ActionsPane.Controls.Add(_MaterialSelectorPaneControl);

            _LogonActionsPaneControl.Hide();
            _QueryProjectsActionPaneControl.Hide();
            _CreateProjectBaselineActionsPaneControl.Hide();
            _MaterialSelectorPaneControl.Hide();

            Globals.ThisWorkbook.Application.DisplayDocumentActionTaskPane = false;

            if (ThisWorkbook.excelAddInMode == ExcelAddInMode.ProjectExpenseMode)
            {
                Globals.ThisWorkbook.SheetActivate += ThisWorkbook_SheetActivate;

                SaveProjectButton.Visible = true;
                CreateProjectBaselineBtn.Visible = true;
                QueryProjectButton.Visible = true;

                UploadTaskExpenseBtn.Visible = false;
                UploadMaterialsInSAPBtn.Visible = false;
                CreateMaterialEntriesBtn.Visible = false;
            }
            else
            {
                if (ThisWorkbook.excelAddInMode == ExcelAddInMode.MaterialValuationMode)
                {
                    SaveProjectButton.Visible = false;
                    CreateProjectBaselineBtn.Visible = false;
                    QueryProjectButton.Visible = false;
                    UploadTaskExpenseBtn.Visible = false;

                    UploadMaterialsInSAPBtn.Visible = true;
                    CreateMaterialEntriesBtn.Visible = true;
                }
            }
        }

        private void ThisWorkbook_SheetActivate(object Sh)
        {
            if (Type.ReferenceEquals(Sh, Globals.ThisWorkbook.Sheets[1]))
            {
                SaveProjectButton.Visible = true;
                CreateProjectBaselineBtn.Visible = true;
                QueryProjectButton.Visible = true;

                UploadTaskExpenseBtn.Visible = false;
            }
            else
            {
                if (Type.ReferenceEquals(Sh, Globals.ThisWorkbook.Sheets[2]))
                {
                    UploadTaskExpenseBtn.Visible = true;

                    SaveProjectButton.Visible = false;
                    CreateProjectBaselineBtn.Visible = false;
                    QueryProjectButton.Visible = false;
                }
            }
        }

        private void RefreshLookupsButton_Click(object sender, RibbonControlEventArgs e)
        {
            var _SAPLogInSession = SAPLogInSession.Instance;
            if (MessageBox.Show(AppConstants.Refresh_Lookups_From_SAP, "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
            {
                if (_SAPLogInSession.IsLoggedIntoSAP == true)
                {
                    if (ThisWorkbook.excelAddInMode == ExcelAddInMode.ProjectExpenseMode)
                    {
                        Cursor.Current = Cursors.WaitCursor;
                        var _ManageSAPCodeList = new ManageSAPCodeList();
                        _ManageSAPCodeList.RefreshAllCodesFromSAP();


                        var _ManageEmployeesLookupsObj = new ManageEmployeesLookup();
                        _ManageEmployeesLookupsObj.RefreshEmployeesListFromSAP("*");

                        var _ManageServiceProductsLookupObj = new ManageServiceProductsLookup();
                        _ManageServiceProductsLookupObj.RefreshServiceProductsListFromSAP("*");

                        var _ManageProjectsLookupListObj = new ManageProjectsLookupList();
                        _ManageProjectsLookupListObj.RefreshProjectsListFromSAP("*");

                        Globals.Sheet1.LoadLookupLists();
                        Globals.ProjectExpenseUploadSheet.LoadLookupLists();

                        Cursor.Current = Cursors.Default;
                        MessageBox.Show(AppConstants.Lookup_Lists_Loadded, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        if (ThisWorkbook.excelAddInMode == ExcelAddInMode.MaterialValuationMode)
                        {
                            Cursor.Current = Cursors.WaitCursor;
                            ManageMaterialLookup _ManageMaterialLookup = new ManageMaterialLookup();
                            _ManageMaterialLookup.QueryFromSAPAndSaveMaterials();

                            ManageSAPCodeList _ManageSAPCodeList = new ManageSAPCodeList();
                            _ManageSAPCodeList.RefreshAllCodesFromSAP();
                            Cursor.Current = Cursors.Default;
                        }
                    }
                }
                else
                {
                    LogonButton_Click(sender, e);
                }
            }
        }

        private void QueryProjectButton_Click(object sender, RibbonControlEventArgs e)
        {
            var _SAPLogInSession = SAPLogInSession.Instance;
            if (_SAPLogInSession.IsLoggedIntoSAP == true)
            {
                Globals.ThisWorkbook.Application.DisplayDocumentActionTaskPane = true;

                _LogonActionsPaneControl.Hide();
                _CreateProjectBaselineActionsPaneControl.Hide();
                _QueryProjectsActionPaneControl.Show();
            }
            else
            {
                LogonButton_Click(sender, e);
            }
        }

        private void LogonButton_Click(object sender, RibbonControlEventArgs e)
        {
            var _SAPLogInSession = SAPLogInSession.Instance;
            if (_SAPLogInSession.IsLoggedIntoSAP == false)
            {
                Globals.ThisWorkbook.Application.DisplayDocumentActionTaskPane = true;
                _QueryProjectsActionPaneControl.Hide();
                _CreateProjectBaselineActionsPaneControl.Hide();
                _LogonActionsPaneControl.Show();
            }
        }

        private void CreateProjectBaselineBtn_Click(object sender, RibbonControlEventArgs e)
        {
            var _SAPLogInSession = SAPLogInSession.Instance;
            if (_SAPLogInSession.IsLoggedIntoSAP == true)
            {
                Globals.ThisWorkbook.Application.DisplayDocumentActionTaskPane = true;
                _LogonActionsPaneControl.Hide();
                _QueryProjectsActionPaneControl.Hide();
                _CreateProjectBaselineActionsPaneControl.Show();
            }
            else
            {
                LogonButton_Click(sender, e);
            }
        }

        private void SaveProjectButton_Click(object sender, RibbonControlEventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            var _SAPProjectsData = Globals.Sheet1.GetProjectToUpdateInSAP();
            var ObjList = new List<SAPProjectsData>();
            ObjList.Add(_SAPProjectsData);
            var ResList = SAPProjectUpdater.UpdateProjectInSAP(ObjList);


            var SB = new StringBuilder();
            foreach (var item in ResList)
            {
                if (item.IsSucessfull == false)
                {
                    SB.Append(String.Format("{0}ProjectID: {1}, {0} Error Message: {2}", Environment.NewLine, item.ProjectID, item.Response));
                }
            }
            if (SB.Length > 0)
            {
                MessageBox.Show(SB.ToString());
            }
            var SB1 = new StringBuilder();
            foreach (var item in ResList)
            {
                if (item.IsSucessfull == true)
                {
                    SB1.Append(String.Format("Project {0} Updated Sucessfully in SAP {1}", item.ProjectID, Environment.NewLine));
                }
            }

            Cursor.Current = Cursors.Default;

            if (SB1.Length > 0)
            {
                MessageBox.Show(SB1.ToString());
            }
        }

        private void UploadTaskExpenseBtn_Click(object sender, RibbonControlEventArgs e)
        {
            var _SAPLogInSession = SAPLogInSession.Instance;
            if (_SAPLogInSession.IsLoggedIntoSAP == false)
            {
                MessageBox.Show(AppConstants.Not_Logged_into_SAP, "Not Logged in", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                return;
            }

            var _ProjectTaskUploadList = Globals.ProjectExpenseUploadSheet.GetListOfEnteredObjects();
            if (_ProjectTaskUploadList.Count == 0)
            {
                MessageBox.Show("Please enter Project Expense line items to proceed......");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            var FilePathStr = CreateProjectExpenseUploadXMLDocument.CreateAndGetProjectExpenseUploadDocument(_ProjectTaskUploadList);
            if (File.Exists(FilePathStr) == true)
            {
                var res = UploadXMLFileToSAPWebDevFolder.UploadXMLFileInSAP(FilePathStr);
                if (res == true)
                {
                    MessageBox.Show("File uploaded to SAP Sucessfully");
                }
                else
                {
                    MessageBox.Show("Unable to upload file to SAP due to the some error");
                }
            }
            Cursor.Current = Cursors.Default;
        }

        private void UploadMaterialsInSAPBtn_Click(object sender, RibbonControlEventArgs e)
        {
             var _SAPLogInSession = SAPLogInSession.Instance;
             if (MessageBox.Show(AppConstants.Upload_Material_Valuation_In_SAP, "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
             {
                 if (_SAPLogInSession.IsLoggedIntoSAP == true)
                 {
                     Globals.MaterialValuationUploadSheet.CreateAllMaterialObjectsInSAP();
                 }
                 else
                     LogonButton_Click(sender, e);
             }
        }

        private void CreateMaterialEntriesBtn_Click(object sender, RibbonControlEventArgs e)
        {
            var _SAPLogInSession = SAPLogInSession.Instance;
            if (_SAPLogInSession.IsLoggedIntoSAP == true)
            {
                Globals.ThisWorkbook.Application.DisplayDocumentActionTaskPane = true;
                _LogonActionsPaneControl.Hide();
                _MaterialSelectorPaneControl.Show();
            }
            else
            {
                LogonButton_Click(sender, e);
            }
        }
    }
}
