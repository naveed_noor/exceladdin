﻿namespace SpirationProjectExpenseExcelWorkbook
{
    [System.ComponentModel.ToolboxItemAttribute(false)]
    partial class QueryProjectsActionPaneControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.ProjectIDTxt = new System.Windows.Forms.TextBox();
            this.QueryProjectsFromSAPBtn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Project ID";
            // 
            // ProjectIDTxt
            // 
            this.ProjectIDTxt.Location = new System.Drawing.Point(10, 28);
            this.ProjectIDTxt.Name = "ProjectIDTxt";
            this.ProjectIDTxt.Size = new System.Drawing.Size(173, 20);
            this.ProjectIDTxt.TabIndex = 1;
            // 
            // QueryProjectsFromSAPBtn
            // 
            this.QueryProjectsFromSAPBtn.Location = new System.Drawing.Point(10, 54);
            this.QueryProjectsFromSAPBtn.Name = "QueryProjectsFromSAPBtn";
            this.QueryProjectsFromSAPBtn.Size = new System.Drawing.Size(173, 23);
            this.QueryProjectsFromSAPBtn.TabIndex = 2;
            this.QueryProjectsFromSAPBtn.Text = "Query SAP Project(s)";
            this.QueryProjectsFromSAPBtn.UseVisualStyleBackColor = true;
            this.QueryProjectsFromSAPBtn.Click += new System.EventHandler(this.QueryProjectsFromSAPBtn_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 84);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(173, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "use * for multiple projects searching";
            // 
            // QueryProjectsActionPaneControl
            // 
            this.Controls.Add(this.label2);
            this.Controls.Add(this.QueryProjectsFromSAPBtn);
            this.Controls.Add(this.ProjectIDTxt);
            this.Controls.Add(this.label1);
            this.Name = "QueryProjectsActionPaneControl";
            this.Size = new System.Drawing.Size(202, 118);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox ProjectIDTxt;
        private System.Windows.Forms.Button QueryProjectsFromSAPBtn;
        private System.Windows.Forms.Label label2;
    }
}
