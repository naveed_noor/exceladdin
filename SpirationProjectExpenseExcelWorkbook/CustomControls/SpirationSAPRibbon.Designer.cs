﻿namespace SpirationProjectExpenseExcelWorkbook
{
    partial class SpirationSAPRibbon : Microsoft.Office.Tools.Ribbon.RibbonBase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public SpirationSAPRibbon()
            : base(Globals.Factory.GetRibbonFactory())
        {
            InitializeComponent();
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SpirationSAPRibbon));
            this.SpirationSAPProjectTab = this.Factory.CreateRibbonTab();
            this.group1 = this.Factory.CreateRibbonGroup();
            this.LogonButton = this.Factory.CreateRibbonButton();
            this.SaveProjectButton = this.Factory.CreateRibbonButton();
            this.UploadTaskExpenseBtn = this.Factory.CreateRibbonButton();
            this.UploadMaterialsInSAPBtn = this.Factory.CreateRibbonButton();
            this.group2 = this.Factory.CreateRibbonGroup();
            this.RefreshLookupsButton = this.Factory.CreateRibbonButton();
            this.QueryProjectButton = this.Factory.CreateRibbonButton();
            this.CreateProjectBaselineBtn = this.Factory.CreateRibbonButton();
            this.CreateMaterialEntriesBtn = this.Factory.CreateRibbonButton();
            this.SpirationSAPProjectTab.SuspendLayout();
            this.group1.SuspendLayout();
            this.group2.SuspendLayout();
            // 
            // SpirationSAPProjectTab
            // 
            this.SpirationSAPProjectTab.Groups.Add(this.group1);
            this.SpirationSAPProjectTab.Groups.Add(this.group2);
            this.SpirationSAPProjectTab.Label = "MindSight";
            this.SpirationSAPProjectTab.Name = "SpirationSAPProjectTab";
            // 
            // group1
            // 
            this.group1.Items.Add(this.LogonButton);
            this.group1.Items.Add(this.SaveProjectButton);
            this.group1.Items.Add(this.UploadTaskExpenseBtn);
            this.group1.Items.Add(this.UploadMaterialsInSAPBtn);
            this.group1.Label = "SAP";
            this.group1.Name = "group1";
            // 
            // LogonButton
            // 
            this.LogonButton.Image = global::SpirationProjectExpenseExcelWorkbook.Properties.Resources.SAP_Logon;
            this.LogonButton.Label = "Logon";
            this.LogonButton.Name = "LogonButton";
            this.LogonButton.ShowImage = true;
            this.LogonButton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.LogonButton_Click);
            // 
            // SaveProjectButton
            // 
            this.SaveProjectButton.Image = ((System.Drawing.Image)(resources.GetObject("SaveProjectButton.Image")));
            this.SaveProjectButton.Label = "Save Project";
            this.SaveProjectButton.Name = "SaveProjectButton";
            this.SaveProjectButton.ShowImage = true;
            this.SaveProjectButton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.SaveProjectButton_Click);
            // 
            // UploadTaskExpenseBtn
            // 
            this.UploadTaskExpenseBtn.Image = global::SpirationProjectExpenseExcelWorkbook.Properties.Resources.upload_file;
            this.UploadTaskExpenseBtn.Label = "Upload to SAP";
            this.UploadTaskExpenseBtn.Name = "UploadTaskExpenseBtn";
            this.UploadTaskExpenseBtn.ShowImage = true;
            this.UploadTaskExpenseBtn.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.UploadTaskExpenseBtn_Click);
            // 
            // UploadMaterialsInSAPBtn
            // 
            this.UploadMaterialsInSAPBtn.Image = global::SpirationProjectExpenseExcelWorkbook.Properties.Resources.upload_file;
            this.UploadMaterialsInSAPBtn.Label = "Upload Materials Valuation in SAP";
            this.UploadMaterialsInSAPBtn.Name = "UploadMaterialsInSAPBtn";
            this.UploadMaterialsInSAPBtn.ShowImage = true;
            this.UploadMaterialsInSAPBtn.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.UploadMaterialsInSAPBtn_Click);
            // 
            // group2
            // 
            this.group2.Items.Add(this.RefreshLookupsButton);
            this.group2.Items.Add(this.QueryProjectButton);
            this.group2.Items.Add(this.CreateProjectBaselineBtn);
            this.group2.Items.Add(this.CreateMaterialEntriesBtn);
            this.group2.Label = "Project";
            this.group2.Name = "group2";
            // 
            // RefreshLookupsButton
            // 
            this.RefreshLookupsButton.Image = global::SpirationProjectExpenseExcelWorkbook.Properties.Resources.Refresh;
            this.RefreshLookupsButton.Label = "Refresh Lookups";
            this.RefreshLookupsButton.Name = "RefreshLookupsButton";
            this.RefreshLookupsButton.ShowImage = true;
            this.RefreshLookupsButton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.RefreshLookupsButton_Click);
            // 
            // QueryProjectButton
            // 
            this.QueryProjectButton.Image = global::SpirationProjectExpenseExcelWorkbook.Properties.Resources.query_projects;
            this.QueryProjectButton.Label = "Query Project(s)";
            this.QueryProjectButton.Name = "QueryProjectButton";
            this.QueryProjectButton.ShowImage = true;
            this.QueryProjectButton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.QueryProjectButton_Click);
            // 
            // CreateProjectBaselineBtn
            // 
            this.CreateProjectBaselineBtn.Image = global::SpirationProjectExpenseExcelWorkbook.Properties.Resources.baseline_icon;
            this.CreateProjectBaselineBtn.Label = "Create Project Baseline";
            this.CreateProjectBaselineBtn.Name = "CreateProjectBaselineBtn";
            this.CreateProjectBaselineBtn.ShowImage = true;
            this.CreateProjectBaselineBtn.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.CreateProjectBaselineBtn_Click);
            // 
            // CreateMaterialEntriesBtn
            // 
            this.CreateMaterialEntriesBtn.Image = global::SpirationProjectExpenseExcelWorkbook.Properties.Resources.investval_icon;
            this.CreateMaterialEntriesBtn.Label = "Create Material Entries";
            this.CreateMaterialEntriesBtn.Name = "CreateMaterialEntriesBtn";
            this.CreateMaterialEntriesBtn.ShowImage = true;
            this.CreateMaterialEntriesBtn.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.CreateMaterialEntriesBtn_Click);
            // 
            // SpirationSAPRibbon
            // 
            this.Name = "SpirationSAPRibbon";
            this.RibbonType = "Microsoft.Excel.Workbook";
            this.Tabs.Add(this.SpirationSAPProjectTab);
            this.Load += new Microsoft.Office.Tools.Ribbon.RibbonUIEventHandler(this.SpirationSAPRibbon_Load);
            this.SpirationSAPProjectTab.ResumeLayout(false);
            this.SpirationSAPProjectTab.PerformLayout();
            this.group1.ResumeLayout(false);
            this.group1.PerformLayout();
            this.group2.ResumeLayout(false);
            this.group2.PerformLayout();

        }

        #endregion

        internal Microsoft.Office.Tools.Ribbon.RibbonTab SpirationSAPProjectTab;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup group1;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton LogonButton;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup group2;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton RefreshLookupsButton;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton QueryProjectButton;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton SaveProjectButton;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton CreateProjectBaselineBtn;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton UploadTaskExpenseBtn;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton UploadMaterialsInSAPBtn;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton CreateMaterialEntriesBtn;
    }

    partial class ThisRibbonCollection
    {
        internal SpirationSAPRibbon SpirationSAPRibbon
        {
            get { return this.GetRibbon<SpirationSAPRibbon>(); }
        }
    }
}
