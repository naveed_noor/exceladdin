﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml.Linq;
using Microsoft.Office.Tools.Excel;
using Microsoft.VisualStudio.Tools.Applications.Runtime;
using Excel = Microsoft.Office.Interop.Excel;
using Office = Microsoft.Office.Core;
using Microsoft.Office.Interop.Excel;

namespace SpirationProjectExpenseExcelWorkbook
{
    public partial class Sheet1
    {
        //  Range cell = (Range)this.get_Range(this.Cells[1, 8], this.Cells[65536, 8]);

        private void Sheet1_Startup(object sender, System.EventArgs e)
        {
            if (ThisWorkbook.excelAddInMode == ExcelAddInMode.ProjectExpenseMode)
            {
                LoadLookupLists();
            }
        }

        public void LoadLookupLists()
        {
            ManageSAPCodeList _ManageSAPCodeList = new ManageSAPCodeList();
            _ManageSAPCodeList.PullAllCodesFromSAP();
            
            CreateLookupList("GLCodesColumn", _ManageSAPCodeList.GetCodesStringList(_ManageSAPCodeList.GetGeneralLedgerAccountAliasCodes()), 
                                                                                    "GL Code Selection List",
                                                                                    "Please Select GL Code from the dropdown list",
                                                                                    "Invalid Selection","Selected/Entered GL Code doesn't exists in the dropdown list");

            CreateLookupList("CurrencyLookupColumn", _ManageSAPCodeList.GetCodesStringList(_ManageSAPCodeList.GetCurrencyCodes()),"","","","");
            CreateLookupList("ExpensePlanPeriodCurrencyLookup", _ManageSAPCodeList.GetCodesStringList(_ManageSAPCodeList.GetCurrencyCodes()), "", "", "", "");
            
        }

        public void SetSheetProjectsData(List<SAPProjectsData> _SAPProjectsData)
        {
            this.sapProjectsDataBindingSource.DataSource = _SAPProjectsData;
            Globals.ThisWorkbook.Application.DisplayDocumentActionTaskPane = false;
        }

        public SAPProjectsData GetProjectToUpdateInSAP()
        {
            return (SAPProjectsData)this.sapProjectsDataBindingSource.Current;
        }

        private void CreateLookupList(String ColumnName, List<String> ValuesList, String MessageTitle, String MessageText, String ErrorMessageTitle, String ErrorMessageText)
        {
            var flatlist = String.Join(",", ValuesList);
            if (flatlist.Equals("") || flatlist.Length <= 0)
                return;

            Range cell = this.get_Range(ColumnName);
            cell.Validation.Delete();
            cell.Validation.Add(XlDVType.xlValidateList, XlDVAlertStyle.xlValidAlertStop, XlFormatConditionOperator.xlBetween, flatlist, System.Type.Missing);

            cell.Validation.ErrorTitle = ErrorMessageTitle;
            cell.Validation.ErrorMessage = ErrorMessageText;

            cell.Validation.InputTitle = MessageTitle;
            cell.Validation.InputMessage = MessageText;

            cell.Validation.InCellDropdown = true;
            
            cell.Validation.IgnoreBlank = true;
            cell.Validation.InCellDropdown = true;
        }


        private void Sheet1_Shutdown(object sender, System.EventArgs e)
        {

        }

        #region VSTO Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(Sheet1_Startup);
            this.Shutdown += new System.EventHandler(Sheet1_Shutdown);
        }

        #endregion

    }
}
