﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml.Linq;
using Microsoft.Office.Tools.Excel;
using Microsoft.VisualStudio.Tools.Applications.Runtime;
using Excel = Microsoft.Office.Interop.Excel;
using Office = Microsoft.Office.Core;

namespace SpirationProjectExpenseExcelWorkbook
{

    public enum ExcelAddInMode{
        
        ProjectExpenseMode = 0,
        MaterialValuationMode = 1
    }

    public partial class ThisWorkbook
    {

        public const ExcelAddInMode excelAddInMode = ExcelAddInMode.MaterialValuationMode;
        
        private void ThisWorkbook_Startup(object sender, System.EventArgs e)
        {
            if (ThisWorkbook.excelAddInMode == ExcelAddInMode.ProjectExpenseMode)
            {
                Globals.MaterialValuationUploadSheet.Visible = Excel.XlSheetVisibility.xlSheetVeryHidden;
                Globals.Sheet1.Visible = Excel.XlSheetVisibility.xlSheetVisible;
                Globals.ProjectExpenseUploadSheet.Visible = Excel.XlSheetVisibility.xlSheetVisible;
                Globals.ValidationSheet.Visible = Excel.XlSheetVisibility.xlSheetVeryHidden;
                Globals.Sheet1.Activate();
            }
            else if (excelAddInMode == ExcelAddInMode.MaterialValuationMode)
            {
                Globals.Sheet1.Visible = Excel.XlSheetVisibility.xlSheetVeryHidden;
                Globals.ProjectExpenseUploadSheet.Visible = Excel.XlSheetVisibility.xlSheetVeryHidden;
                Globals.MaterialValuationUploadSheet.Visible = Excel.XlSheetVisibility.xlSheetVisible;
                Globals.ValidationSheet.Visible = Excel.XlSheetVisibility.xlSheetHidden;
                Globals.MaterialValuationUploadSheet.Activate();
            }
        }


        private void ThisWorkbook_Shutdown(object sender, System.EventArgs e)
        {
        }

        #region VSTO Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisWorkbook_Startup);
            this.Shutdown += new System.EventHandler(ThisWorkbook_Shutdown);
        }

        #endregion

    }
}
