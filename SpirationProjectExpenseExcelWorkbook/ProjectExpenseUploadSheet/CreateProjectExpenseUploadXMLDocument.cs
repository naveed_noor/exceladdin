﻿using SpirationProjectExpenseExcelWorkbook.SAPServices.ObjectManupilationClasses;
using SpirationProjectExpenseExcelWorkbook.SAPServices.SAPServicesManagementClasses;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml; 

namespace SpirationProjectExpenseExcelWorkbook
{
    public class CreateProjectExpenseUploadXMLDocument
    {
        public static String CreateAndGetProjectExpenseUploadDocument(List<ProjectTaskUpload> _ProjectTaskUploadObjList)
        {
            string path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

            String XMLFilePath = string.Format("{0}\\ProjectTaskUpload.xml", path);

            XmlTextWriter writer = new XmlTextWriter(XMLFilePath, System.Text.Encoding.UTF8);
            writer.WriteStartDocument(true);
            writer.Formatting = Formatting.Indented;
            writer.Indentation = 2;

            writer.WriteStartElement("ns1", "ProjectTaskServiceUploadInputRequest", "http://0012710951-one-off.sap.com/YJAKCXOWY_");

            writer.WriteStartElement("MessageHeader");
                    writer.WriteStartElement("CreationDateTime");
                        writer.WriteString(DateTime.Now.ToUniversalTime().ToString("yyyy-MM-dd") + "T09:00:00Z");
                    writer.WriteEndElement(); // End of CreationDateTime
            writer.WriteEndElement(); // End of MessageHeader

            writer.WriteStartElement("List");

            foreach (ProjectTaskUpload _ProjectTaskUploadObj in _ProjectTaskUploadObjList)
            {
                String AssignedEmployeeID = _ProjectTaskUploadObj.AssignedEmployeeID.Substring(_ProjectTaskUploadObj.AssignedEmployeeID.IndexOf("-")+1).Trim();
                String ProductID = _ProjectTaskUploadObj.ProductID.Substring(_ProjectTaskUploadObj.ProductID.IndexOf("-")+1).Trim();
                String PlannedPeriodStartDate = "", PlannedPeriodEndDate = "";
                if(_ProjectTaskUploadObj.PlannedPeriodStartDate.ToString("yyyy") != "0001")
                    PlannedPeriodStartDate = _ProjectTaskUploadObj.PlannedPeriodStartDate.ToUniversalTime().ToString("yyyy-MM-dd") + "T09:00:00Z";
                if (_ProjectTaskUploadObj.PlannedPeriodEndDate.ToString("yyyy") != "0001")
                    PlannedPeriodEndDate = _ProjectTaskUploadObj.PlannedPeriodEndDate.ToUniversalTime().ToString("yyyy-MM-dd") + "T09:00:00Z";

                createNode(_ProjectTaskUploadObj.ProjectID, _ProjectTaskUploadObj.ProjectTaskID, _ProjectTaskUploadObj.ProductTypeCode.ToString(),
                            _ProjectTaskUploadObj.ProductIdentifierTypeCode.ToString(),
                            ProductID, AssignedEmployeeID, 
                            _ProjectTaskUploadObj.PlannedWorkQuantity.ToString(),
                            PlannedPeriodStartDate,
                            PlannedPeriodEndDate, 
                            writer);
            }

            writer.WriteEndElement(); // End of List Element
            writer.WriteEndElement(); // end of ProjectTaskServiceUploadInputRequest Element
            writer.WriteEndDocument();
            writer.Close();

            return XMLFilePath;
        }

        private static void createNode(string ProjectID, string TaskID, string ProductTypeCode, string ProductIdentifierTypeCode, String ProductID, String AssignedEmployeeID,
                                String PlannedWorkQuantity, String StartDateTime, String EndDateTime, XmlTextWriter writer)
        {
            writer.WriteStartElement("Project");
                writer.WriteStartElement("ProjectID");
                    writer.WriteString(ProjectID);
                    writer.WriteEndElement();

                        writer.WriteStartElement("Task");

                            writer.WriteStartElement("ProjectTaskKey");
                                writer.WriteStartElement("TaskID");
                                    writer.WriteString(TaskID);
                                writer.WriteEndElement(); // end of TaskID
                            writer.WriteEndElement(); // End of ProjectTaskKey

                            writer.WriteStartElement("TaskService");
                                writer.WriteStartElement("ServiceProductKey");

                                    writer.WriteStartElement("ProductTypeCode");
                                    writer.WriteString(ProductTypeCode);
                                    writer.WriteEndElement(); // end of ProductTypeCode                        

                                    writer.WriteStartElement("ProductIdentifierTypeCode");
                                    writer.WriteString(ProductIdentifierTypeCode);
                                    writer.WriteEndElement(); // end of ProductIdentifierTypeCode

                                    writer.WriteStartElement("ProductID");
                                    writer.WriteString(ProductID);
                                    writer.WriteEndElement(); // end of ProductID                        

                                writer.WriteEndElement(); // End of ServiceProductKey

                                writer.WriteStartElement("AssignedEmployeeID");
                                writer.WriteString(AssignedEmployeeID);
                                writer.WriteEndElement(); // end of AssignedEmployeeID

                                writer.WriteStartElement("PlannedWorkQuantity");
                                writer.WriteString(PlannedWorkQuantity);
                                writer.WriteEndElement(); // end of PlannedWorkQuantity

                                if (!StartDateTime.Equals("") && !EndDateTime.Equals(""))
                                {
                                    writer.WriteStartElement("PlannedPeriod");

                                    writer.WriteStartElement("StartDateTime");
                                        writer.WriteAttributeString("timeZoneCode", "UTC");
                                        writer.WriteString(StartDateTime);
                                    writer.WriteEndElement(); // end of StartDateTime

                                    writer.WriteStartElement("EndDateTime");
                                        writer.WriteAttributeString("timeZoneCode", "UTC");
                                        writer.WriteString(EndDateTime);
                                    writer.WriteEndElement(); // end of EndDateTime

                                    writer.WriteEndElement(); // End of PlannedPeriod
                                }
                            writer.WriteEndElement(); // End of TaskService

                        writer.WriteEndElement(); // End of Task

            writer.WriteEndElement(); // End of Project Tag
        }
    }
}
