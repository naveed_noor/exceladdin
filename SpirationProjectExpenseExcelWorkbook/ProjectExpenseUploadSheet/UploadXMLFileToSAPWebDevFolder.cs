﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.Windows.Forms;

namespace SpirationProjectExpenseExcelWorkbook
{
    public class UploadXMLFileToSAPWebDevFolder
    {

        public static Boolean UploadXMLFileInSAP(String FilePath)
        {
            Boolean res = CreateWebDevFile(FilePath);
            return res;
        }

        private static Boolean CreateWebDevFile(String FilePath)
        {
            try
            {
                SAPLogInSession _SAPLogInSession = SAPLogInSession.Instance;
                // Define the URLs.
                string szURL1 = @"https://my" + _SAPLogInSession.TanentID + 
                                            ".sapbydesign.com/sap/a1s/cd/fileio/docs/in/0012710951-one-off.sap.com_YJAKCXOWY__/ProjectTaskServiceUploadInput/ProjectTaskUploadFile.xml";
               

                // Define username and password strings.
                string szUsername = _SAPLogInSession.Username;
                string szPassword = _SAPLogInSession.Password;

                // Create an HTTP request for the URL.
                HttpWebRequest httpPutRequest = (HttpWebRequest)WebRequest.Create(szURL1);

                // Set up new credentials.
                httpPutRequest.Credentials = new NetworkCredential(szUsername, szPassword);

                // Pre-authenticate the request.
                httpPutRequest.PreAuthenticate = true;

                // Define the HTTP method.
                httpPutRequest.Method = @"PUT";

                // Specify that overwriting the destination is allowed.
                httpPutRequest.Headers.Add(@"Overwrite", @"T");
               
                // Optional, but allows for larger files.
                httpPutRequest.SendChunked = true;

                // Read File 
                var data = File.ReadAllBytes(FilePath);

                // Specify the content length.
                httpPutRequest.ContentLength = data.Length;

                // Retrieve the request stream.
                Stream requestStream = httpPutRequest.GetRequestStream();


                // Write the string to the destination as a text file.
                requestStream.Write(data, 0, data.Length);

                // Close the request stream.
                requestStream.Close();

                // Retrieve the response.
                HttpWebResponse httpPutResponse = (HttpWebResponse)httpPutRequest.GetResponse();

                if (httpPutResponse != null && httpPutResponse.StatusCode == HttpStatusCode.OK)
                    return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return false;
        }


        private static Boolean IsWebDevFilePresent()
        {
            try
            {
                SAPLogInSession _SAPLogInSession = SAPLogInSession.Instance;
                // Define the URLs.
                string szURL1 = @"https://my" + _SAPLogInSession.TanentID +
                            ".sapbydesign.com/sap/a1s/cd/fileio/docs/in/0012710951-one-off.sap.com_YJAKCXOWY__/ProjectTaskServiceUploadInput/ProjectTaskUploadFile.xml";

                // Define username and password strings.
                string szUsername = _SAPLogInSession.Username;
                string szPassword = _SAPLogInSession.Password;

                // Create an HTTP request for the URL.
                HttpWebRequest httpGetRequest = (HttpWebRequest)WebRequest.Create(szURL1);

                // Set up new credentials.
                httpGetRequest.Credentials = new NetworkCredential(szUsername, szPassword);

                // Pre-authenticate the request.
                httpGetRequest.PreAuthenticate = true;

                // Define the HTTP method.
                httpGetRequest.Method = @"GET";

                // Specify the request for source code.
                httpGetRequest.Headers.Add(@"Translate", "F");

                // Retrieve the response.
                HttpWebResponse httpGetResponse = (HttpWebResponse)httpGetRequest.GetResponse();

                //// Retrieve the response stream.
                //Stream responseStream = httpGetResponse.GetResponseStream();

                //// Retrieve the response length.
                //long responseLength = httpGetResponse.ContentLength;

                //// Create a stream reader for the response.
                //StreamReader streamReader = new StreamReader(responseStream, Encoding.UTF8);
                
                //// Close the response streams.
                //streamReader.Close();
                //responseStream.Close();

                if (httpGetResponse != null && httpGetResponse.StatusCode == HttpStatusCode.OK)
                    return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return false;
        }

    }
}
