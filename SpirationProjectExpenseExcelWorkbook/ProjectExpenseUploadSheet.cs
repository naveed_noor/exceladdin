﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml.Linq;
using Microsoft.Office.Tools.Excel;
using Microsoft.VisualStudio.Tools.Applications.Runtime;
using Excel = Microsoft.Office.Interop.Excel;
using Office = Microsoft.Office.Core;
using Microsoft.Office.Interop.Excel;
using SpirationProjectExpenseExcelWorkbook.SAPServices.SAPWebServiceClasses;
using SpirationProjectExpenseExcelWorkbook.SAPServices.ObjectManupilationClasses;
using SpirationProjectExpenseExcelWorkbook.SAPServices.SAPServicesManagementClasses;
using System.Reflection;

namespace SpirationProjectExpenseExcelWorkbook
{
    public partial class ProjectExpenseUploadSheet
    {
        private void Sheet2_Startup(object sender, System.EventArgs e)
        {
            if (ThisWorkbook.excelAddInMode == ExcelAddInMode.ProjectExpenseMode)
            {
                LoadLookupLists();
            }
        }

        List<ProjectsList> ProjectsListList = null;

        public List<ProjectTaskUpload> GetListOfEnteredObjects()
        {
            List<ProjectTaskUpload> _ProjectTaskUploadList = new List<ProjectTaskUpload>();

            foreach (var item in this.projectTaskUploadBindingSource.List)
            {
                _ProjectTaskUploadList.Add((ProjectTaskUpload)item);
            }
            
            

            return _ProjectTaskUploadList;
        }


        public void LoadLookupLists()
        {
            ManageEmployeesLookup _ManageEmployeesLookupsObj = new ManageEmployeesLookup();
            List<SAPEmployeesData> SAPEmployeesDataList = _ManageEmployeesLookupsObj.PullEmployees("*");
            if (SAPEmployeesDataList != null)
            {
                CreateLookupList("EmployeesLookupListColumn", _ManageEmployeesLookupsObj.GetEmployeesStringList(SAPEmployeesDataList),
                                                "Assigned Employees List",
                                                "Please Select Employee from the dropdown list",
                                                "Invalid Selection",
                                                "Selected/Entered Employee doesn't exists in the dropdown list");
            }

            ManageServiceProductsLookup _ManageServiceProductsLookupObj = new ManageServiceProductsLookup();
            List<ServiceProductsData> ServiceProductsDataList = _ManageServiceProductsLookupObj.PullServiceProducts("*");
            if (ServiceProductsDataList != null)
            {
                CreateLookupList("ServiceProductsColumn", _ManageServiceProductsLookupObj.GetServiceProductsStringList(ServiceProductsDataList),
                                                "Service Products Selection List",
                                                "Please Select Service Product from the dropdown list",
                                                "Invalid Selection",
                                                "Selected/Entered Service Product doesn't exists in the dropdown list");
            }

            ManageProjectsLookupList _ManageProjectsLookupListObj = new ManageProjectsLookupList();
            ProjectsListList = _ManageProjectsLookupListObj.PullProjects("*");
            if (ProjectsListList != null)
            {
                CreateLookupList("ProjectsListColumn", _ManageProjectsLookupListObj.GetProjectsStringList(ProjectsListList),
                                               "Projects Selection List",
                                               "Please Select Projects from the dropdown list",
                                               "Invalid Selection",
                                               "Selected/Entered Project doesn't exists in the dropdown list");
            }

        }

        private void CreateLookupList(String ColumnName, List<String> ValuesList, String MessageTitle, String MessageText, String ErrorMessageTitle, String ErrorMessageText)
        {
            var flatlist = String.Join(",", ValuesList);
            if (flatlist.Equals("") || flatlist.Length <= 0)
                return;

            Range cell = this.get_Range(ColumnName);
            cell.Validation.Delete();
            cell.Validation.Add(XlDVType.xlValidateList, XlDVAlertStyle.xlValidAlertStop, XlFormatConditionOperator.xlBetween, flatlist, System.Type.Missing);

            cell.Validation.ErrorTitle = ErrorMessageTitle;
            cell.Validation.ErrorMessage = ErrorMessageText;

            //cell.Validation.InputTitle = MessageTitle;
            //cell.Validation.InputMessage = MessageText;

            cell.Validation.InCellDropdown = true;

            cell.Validation.IgnoreBlank = true;
            cell.Validation.InCellDropdown = true;
        }

        private void CreateDateValidationRule(String ColumnName, DateTime StartDate, DateTime EndDate, 
                        String MessageTitle, String MessageText, String ErrorMessageTitle, String ErrorMessageText)
        {
            Range cell = this.get_Range(ColumnName);
            cell.Validation.Delete();
            cell.Validation.Add(XlDVType.xlValidateDate, XlDVAlertStyle.xlValidAlertStop, XlFormatConditionOperator.xlBetween, StartDate, EndDate);

            cell.Validation.ErrorTitle = ErrorMessageTitle;
            cell.Validation.ErrorMessage = ErrorMessageText;

            //cell.Validation.InputTitle = MessageTitle;
            //cell.Validation.InputMessage = MessageText;

            cell.Validation.IgnoreBlank = true;
            cell.Validation.InCellDropdown = true;
        }

        private void Sheet2_Shutdown(object sender, System.EventArgs e)
        {
        }

        #region VSTO Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.ProjectsListColumn.Change += new Microsoft.Office.Interop.Excel.DocEvents_ChangeEventHandler(this.ProjectsListColumn_Change);
            this.ProjectTasksIDsList.Change += new Microsoft.Office.Interop.Excel.DocEvents_ChangeEventHandler(this.ProjectTasksIDsList_Change);
            this.projectTaskUploadBindingSource.CurrentItemChanged += new System.EventHandler(this.projectTaskUploadBindingSource_CurrentItemChanged);
            this.Startup += new System.EventHandler(this.Sheet2_Startup);
            this.Shutdown += new System.EventHandler(this.Sheet2_Shutdown);

        }

        #endregion

        private void ProjectsListColumn_Change(Range Target)
        {
            if (Target.Worksheet.Name.Equals("Project Expense Upload"))
            {
                String str = Target.get_Address(Missing.Value, Missing.Value, Excel.XlReferenceStyle.xlA1, Missing.Value, Missing.Value);
                if (str.StartsWith("$A$") && Target.Value is String && ProjectsListList != null)
                {
                    CreateProjectTasksLookupList(Target.Value,"");
                }
            }
        }

        private void ProjectTasksIDsList_Change(Range Target)
        {
            if (Target.Worksheet.Name.Equals("Project Expense Upload"))
            {
                String str = Target.get_Address(Missing.Value, Missing.Value, Excel.XlReferenceStyle.xlA1, Missing.Value, Missing.Value);
                if (str.StartsWith("$B$") && Target.Value is String && ProjectsListList != null)
                {
                    //CreateProjectTasksLookupList("",Target.Value);
                    if (Target.Value != null && !Target.Value.Equals(""))
                    {
                        String _ProjectID = "";
                        ProjectTaskUpload _ProjectTaskUploadObj = this.projectTaskUploadBindingSource.Current as ProjectTaskUpload;
                        if (_ProjectTaskUploadObj == null) return;
                        if (_ProjectTaskUploadObj.ProjectID != null)
                            _ProjectID = _ProjectTaskUploadObj.ProjectID;

                        List<DateTime> datesList = ManageProjectsLookupList.GetProjectTasksDates(ProjectsListList, _ProjectID, Target.Value);
                        if (datesList.Count == 2)
                        {
                            String strError = String.Format("Date must be between Tasks Earliest Start Date {0} and Latest End Date {1}", datesList[0], datesList[1]);
                            CreateDateValidationRule(str.Replace("B","F"), datesList[0], datesList[1], "", "", "Error", strError);
                            CreateDateValidationRule(str.Replace("B", "G"), datesList[0], datesList[1], "", "", "Error", strError);
                        }
                    }

                }
            }
        }

        private void CreateProjectTasksLookupList(String ProjectID, String ProjectTasksID)
        {
            if (!ProjectID.Equals(""))
            {
                CreateLookupList("ProjectTasksIDsList", ManageProjectsLookupList.GetProjectTasksStringList(ProjectsListList, ProjectID),
                                                 "Project Tasks Selection List",
                                                 "Please Select Project Tasks from the dropdown list",
                                                 "Invalid Selection",
                                                 "Selected/Entered Project Task doesn't exists in the dropdown list");
            }

            //if (ProjectTasksID != null && !ProjectTasksID.Equals(""))
            //{
            //    String _ProjectID = ProjectID;
            //    if (ProjectID.Equals(""))
            //    {
            //        ProjectTaskUpload _ProjectTaskUploadObj = this.projectTaskUploadBindingSource.Current as ProjectTaskUpload;
            //        if (_ProjectTaskUploadObj == null) return;
            //        if (_ProjectTaskUploadObj.ProjectID != null)
            //            _ProjectID = _ProjectTaskUploadObj.ProjectID;
            //    }

            //    List<DateTime> datesList = ManageProjectsLookupList.GetProjectTasksDates(ProjectsListList, _ProjectID, ProjectTasksID);
            //    if (datesList.Count == 2)
            //    {
            //        String strError = String.Format("Date must be between Tasks Earliest Start Date {0} and Latest End Date {1}", datesList[0], datesList[1]);
            //        CreateDateValidationRule("PlannedPeriodStartDateValidation", datesList[0], datesList[1], "", "", "Error", strError);
            //        CreateDateValidationRule("PlannedPeriodEndDateValidation", datesList[0], datesList[1], "", "", "Error", strError);
            //    }
            //}
        }

        private void projectTaskUploadBindingSource_CurrentItemChanged(object sender, EventArgs e)
        {
            if (((BindingSource)sender).Current is ProjectTaskUpload)
            {
                ProjectTaskUpload _ProjectTaskUploadObj = ((BindingSource)sender).Current as ProjectTaskUpload;
                if (_ProjectTaskUploadObj.ProjectID != null)
                {
                    CreateProjectTasksLookupList(_ProjectTaskUploadObj.ProjectID, _ProjectTaskUploadObj.ProjectTaskID);
                }
            }
        }

       

    }
}
