﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml.Linq;
using Microsoft.Office.Tools.Excel;
using Microsoft.VisualStudio.Tools.Applications.Runtime;
using Excel = Microsoft.Office.Interop.Excel;
using Office = Microsoft.Office.Core;
using Microsoft.Office.Interop.Excel;
using SpirationProjectExpenseExcelWorkbook.SAPServices.SAPWebServiceClasses;
using SpirationProjectExpenseExcelWorkbook.MaterialValuationUpload;

namespace SpirationProjectExpenseExcelWorkbook
{
    public partial class MaterialValuationUploadSheet
    {
        private void Sheet3_Startup(object sender, System.EventArgs e)
        {
            if (ThisWorkbook.excelAddInMode == ExcelAddInMode.MaterialValuationMode)
            {
                LoadLookups();
            }
        }

        private const String DefaultCompanyID = "1  -  Spiration";
        private const String DefaultSetOfBooksID = "SP1";
        private const String DefaultCostType = "1 - Inventory Cost";
        private const String DefaultCurrency = "USD - US Dollar";
        private List<MaterialInfo> MaterialInfoList;
        private List<UoMObject> UoMList = new List<UoMObject>();
        private List<BusinessResidence> BusinessResidenceList = new List<BusinessResidence>();

        public List<BusinessResidence> GetBusinessResidenceList()
        {
            return BusinessResidenceList;
        }

        private void LoadLookups()
        {
            List<String> CompaniesList = new List<string>();
            CompaniesList.Add(DefaultCompanyID);
            CreateLookupList("CompanyIDLookup", CompaniesList, "", "", "", "");

            List<String> SetOfBooksIDList = new List<string>();
            SetOfBooksIDList.Add("SP1");
            CreateLookupList("SetOfBooksIDList", SetOfBooksIDList, "", "", "", "");

            BusinessResidenceList.Add(new BusinessResidence() { BusinessResidenceID = "10", BusinessResidenceName = "Spiration Headquarters" });
            BusinessResidenceList.Add(new BusinessResidence() { BusinessResidenceID = "20", BusinessResidenceName = "Consignment Inventory" });
            BusinessResidenceList.Add(new BusinessResidence() { BusinessResidenceID = "30", BusinessResidenceName = "Clinical Sites" });
            BusinessResidenceList.Add(new BusinessResidence() { BusinessResidenceID = "40", BusinessResidenceName = "Clinical Sites - VAST" });
            List<String> BusinessResidenceIDList = new List<string>();
            foreach (var item in BusinessResidenceList)
                BusinessResidenceIDList.Add(item.BusinessResidenceID + " - " + item.BusinessResidenceName);
            CreateLookupList("BusinessResidenceIDList", BusinessResidenceIDList, "", "", "", "");

           
            List<String> CostTypeList = new List<string>();
            CostTypeList.Add("1 - Inventory Cost");
            CostTypeList.Add("2 - Estimated Cost");
            CostTypeList.Add("5 - Planned Cost");
            CostTypeList.Add("6 - Book Value");
            CostTypeList.Add("7 - Periodic FIFO Cost");
            CreateLookupList("CostTypeList", CostTypeList, "", "", "", "");

            List<String> AccountDeterminationGroupList = new List<string>();
            AccountDeterminationGroupList.Add("3010 - Raw materials");
            AccountDeterminationGroupList.Add("3020 - Supplies");
            AccountDeterminationGroupList.Add("3030 - Semi-finished products");
            AccountDeterminationGroupList.Add("3040 - Finished products");
            AccountDeterminationGroupList.Add("3050 - Trade Goods");
            AccountDeterminationGroupList.Add("Z110 - Non-Production Consumables");
            AccountDeterminationGroupList.Add("Z900 - Low Value Asset");
            CreateLookupList("AccountDeterminationGroupList", AccountDeterminationGroupList, "", "", "", "");
            
            
            ManageSAPCodeList _ManageSAPCodeList = new ManageSAPCodeList();
            CreateLookupList("MaterialValuationCurrencyLookup", _ManageSAPCodeList.GetCodesStringList(_ManageSAPCodeList.GetCurrencyCodes()), "", "", "", "");
            
            List<String> OnitufMeasureList = new List<string>();
            Excel.Range rng = (Excel.Range)Globals.ValidationSheet.get_Range("G2", "H52");
            for (int i = 0; i < rng.Count / 2; i++)
            {
                Excel.Range cell = (Excel.Range)rng[i, 1];
                String val = cell.Value2.ToString();
                cell = (Excel.Range)rng[i, 2];
                String val1 = cell.Value2.ToString();
                if (i > 0)
                {
                    OnitufMeasureList.Add(val + " - " + val1.Replace(",", " "));
                    UoMList.Add(new UoMObject() { Code = val, Value = val1 });
                }
            }
            OnitufMeasureList.Sort();
            CreateLookupList("CostUoMLookupList", OnitufMeasureList, "", "", "", "");
            
            ManageMaterialLookup _ManageMaterialLookup = new ManageMaterialLookup();
            MaterialInfoList = _ManageMaterialLookup.GetMaterialInfo();
            MaterialInfoList.Sort((x, y) => x.MaterialID.CompareTo(y.MaterialID));

            int startRow = 1;
            foreach (var item in MaterialInfoList)
            {
                var cell = Globals.ValidationSheet.Cells[startRow++, 1];
                cell.Value2 = item.MaterialID +  "  -  " + item.Description;
            }
            CreateLookupList("MaterialValuationMaterialIDLookup", null, "", "", "", "");
            
        }

        private void CreateLookupList(String ColumnName, List<String> ValuesList, String MessageTitle, String MessageText, String ErrorMessageTitle, String ErrorMessageText)
        {
            if (ValuesList == null) // In Case of long lists to create reference based lookup from the excel validations Sheet.
            {
                Range cell1 = this.get_Range(ColumnName);
                cell1.Validation.Delete();
                cell1.Validation.Add(XlDVType.xlValidateList, XlDVAlertStyle.xlValidAlertStop, XlFormatConditionOperator.xlBetween, "=Validations!$A:$A", System.Type.Missing);
            }
            else
            {
                var flatlist = String.Join(",", ValuesList);
                if (flatlist.Equals("") || flatlist.Length <= 0)
                    return;

                Range cell = this.get_Range(ColumnName);
                cell.Validation.Delete();
                cell.Validation.Add(XlDVType.xlValidateList, XlDVAlertStyle.xlValidAlertStop, XlFormatConditionOperator.xlBetween, flatlist, System.Type.Missing);

                cell.Validation.ErrorTitle = ErrorMessageTitle;
                cell.Validation.ErrorMessage = ErrorMessageText;

                //cell.Validation.InputTitle = MessageTitle;
                //cell.Validation.InputMessage = MessageText;

                cell.Validation.InCellDropdown = true;

                cell.Validation.IgnoreBlank = true;
                cell.Validation.InCellDropdown = true;
            }
        }

        private void Sheet3_Shutdown(object sender, System.EventArgs e)
        {

        }

        #region VSTO Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.materialValuationObjectListObject.Change += new Microsoft.Office.Tools.Excel.ListObjectChangeHandler(this.materialValuationObjectListObject_Change);
            this.materialValuationObjectListObject.BeforeAddDataBoundRow += new Microsoft.Office.Tools.Excel.BeforeAddDataBoundRowEventHandler(this.materialValuationObjectListObject_BeforeAddDataBoundRow_1);
            this.Startup += new System.EventHandler(this.Sheet3_Startup);
            this.Shutdown += new System.EventHandler(this.Sheet3_Shutdown);

        }

        #endregion

        private void materialValuationObjectListObject_BeforeAddDataBoundRow_1(object sender, BeforeAddDataBoundRowEventArgs e)
        {
            if (e.Item is MaterialValuationObject)
            {
                MaterialValuationObject _MaterialValuationObjectObj = (MaterialValuationObject)e.Item;
                SetDefaultValues(_MaterialValuationObjectObj);
            }
        }

        private DateTime GetValidFromDate()
        {
            DateTime dt = DateTime.Now;
            Range range = Globals.MaterialValuationUploadSheet.get_Range("Default_Validity_From_Date");
            object value = range.Value2;
            if (value != null)
            {
                if (value is double)
                {
                    dt = DateTime.FromOADate((double)value);
                }
                else
                {
                    DateTime.TryParse((string)value, out dt);
                }
            }
            return dt;
        }

        private DateTime GetValidToDate()
        {
            DateTime dt = DateTime.Now;
            Range range = Globals.MaterialValuationUploadSheet.get_Range("Default_Validity_To_Date");
            object value = range.Value2;
            if (value != null)
            {
                if (value is double)
                {
                    dt = DateTime.FromOADate((double)value);
                }
                else
                {
                    DateTime.TryParse((string)value, out dt);
                }
            }
            return dt;
        }

        private void SetDefaultValues(MaterialValuationObject _MaterialValuationObjectObj)
        {
            if (_MaterialValuationObjectObj != null)
            {

                if (_MaterialValuationObjectObj.CompanyID == null)
                    _MaterialValuationObjectObj.CompanyID = DefaultCompanyID;

                if (_MaterialValuationObjectObj.SetOfBooksID == null)
                    _MaterialValuationObjectObj.SetOfBooksID = DefaultSetOfBooksID;

                if (_MaterialValuationObjectObj.CostType == null)
                    _MaterialValuationObjectObj.CostType = DefaultCostType;

                if (_MaterialValuationObjectObj.Currency == null)
                    _MaterialValuationObjectObj.Currency = DefaultCurrency;

                if (_MaterialValuationObjectObj.CostUnit == null)
                    _MaterialValuationObjectObj.CostUnit = "1";
                
                if (_MaterialValuationObjectObj.ValidFrom == null || _MaterialValuationObjectObj.ValidFrom == DateTime.MinValue)
                    _MaterialValuationObjectObj.ValidFrom = GetValidFromDate();

                if (_MaterialValuationObjectObj.ValidTo == null || _MaterialValuationObjectObj.ValidTo == DateTime.MinValue)
                    _MaterialValuationObjectObj.ValidTo = GetValidToDate();

                if (_MaterialValuationObjectObj.MaterialID != null)
                {
                    string Str = GetFirstPart(_MaterialValuationObjectObj.MaterialID);
                    MaterialInfo materialInfoObj = MaterialInfoList.Find(r => r.MaterialID == Str);
                    if (materialInfoObj != null)
                    {
                        UoMObject UoMObjectObj = UoMList.Find(x => x.Code == materialInfoObj.BaseMeasureUnitCode);
                        if (UoMObjectObj != null && _MaterialValuationObjectObj.CostUoM == null)
                        {
                            _MaterialValuationObjectObj.CostUoM = UoMObjectObj.Code + " - " + UoMObjectObj.Value;
                            this.materialValuationObjectListObject.RefreshDataRow(this.materialValuationObjectBindingSource.Position);
                        }
                    }
                }
            }
        }

        private Boolean ValidateMaterialValuationObject(MaterialValuationObject _MaterialValuationObjectObj, int RowNumber)
        {
            if (_MaterialValuationObjectObj != null)
            {
                if (_MaterialValuationObjectObj.CompanyID == null || _MaterialValuationObjectObj.CompanyID.Trim().Equals(""))
                {
                    MessageBox.Show(String.Format("must select/enter Company ID in Row Number '{0}'", RowNumber), "Company ID Missing", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                    return false;
                }

                if (_MaterialValuationObjectObj.SetOfBooksID == null || _MaterialValuationObjectObj.SetOfBooksID.Trim().Equals(""))
                {
                    MessageBox.Show(String.Format("must select/enter Set Of Books ID in Row Number '{0}'", RowNumber), "Set Of Books ID Missing", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                    return false;
                }

                if(_MaterialValuationObjectObj.ValidFrom == DateTime.MinValue)
                {
                    MessageBox.Show(String.Format("must enter Valid From Date in Row Number '{0}'", RowNumber), "Valid From Date Missing", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                    return false;
                }

                if (_MaterialValuationObjectObj.ValidTo == DateTime.MinValue)
                {
                    MessageBox.Show(String.Format("must enter Valid To Date in Row Number '{0}'", RowNumber), "Valid To Date Missing", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                    return false;
                }

                if (_MaterialValuationObjectObj.BusinessResidenceID == null)
                {
                    MessageBox.Show(String.Format("must enter Business Residence ID in Row Number '{0}'", RowNumber), "Business Residence ID Missing", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                    return false;
                }

                if (_MaterialValuationObjectObj.Cost <= 0)
                {
                    MessageBox.Show(String.Format("Cost of Item must be valid in Row Number '{0}'", RowNumber), "Invalid Cost", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                    return false;
                }

                if (_MaterialValuationObjectObj.MaterialID != null)
                {
                    string Str = GetFirstPart(_MaterialValuationObjectObj.MaterialID);
                    MaterialInfo materialInfoObj = MaterialInfoList.Find(r => r.MaterialID == Str);
                    if ((_MaterialValuationObjectObj.AccountDeterminationGroup == null || _MaterialValuationObjectObj.AccountDeterminationGroup == " ") && _MaterialValuationObjectObj.BusinessResidenceID != null)
                    {
                        String str = GetFirstPart(_MaterialValuationObjectObj.BusinessResidenceID);
                        StringBuilder SB = new StringBuilder();

                        ValuationInfo ValuationInfoObj = materialInfoObj.ValuationInfoList.Find(z => z.BusinessResidenceID == str);
                        if (ValuationInfoObj == null)
                        {
                            string MessageStr = String.Format("Business Residence {0} for Material Valuation of the Material '{1}' doesn't exists in SAP {2}", _MaterialValuationObjectObj.BusinessResidenceID, _MaterialValuationObjectObj.MaterialID, Environment.NewLine);
                            MessageStr = MessageStr + Environment.NewLine + String.Format("Please select Account Determination Group for the Material in Row Number '{0}' to create relevant Business Residence Material Valuation entries in SAP", RowNumber);                            
                            MessageBox.Show(MessageStr, "Issue in SAP Material", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                            _MaterialValuationObjectObj.AccountDeterminationGroup = " ";
                            _MaterialValuationObjectObj.IsValuationExistsInSAP = false;
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        public void CreateMaterialRowsForAllBusinessResidences(List<String> materialList, List<String> businessResidenceStringList)
        {
            foreach (String MaterialID in materialList)
            {
                foreach (var item in businessResidenceStringList)
                {
                        MaterialValuationObject _MaterialValuationObj = new MaterialValuationObject();

                        _MaterialValuationObj.CompanyID = DefaultCompanyID;
                        _MaterialValuationObj.SetOfBooksID = DefaultSetOfBooksID;
                        
                        MaterialInfo _MaterialInfoObj = MaterialInfoList.Find(x => x.MaterialID == MaterialID);
                        if (_MaterialInfoObj != null)
                            _MaterialValuationObj.MaterialID = _MaterialInfoObj.MaterialID + " - " + _MaterialInfoObj.Description;

                        BusinessResidence _BusinessResidenceObj = BusinessResidenceList.Find(x => x.BusinessResidenceID == item);
                        if (_BusinessResidenceObj != null)
                            _MaterialValuationObj.BusinessResidenceID = _BusinessResidenceObj.BusinessResidenceID + " - " + _BusinessResidenceObj.BusinessResidenceName;

                        int res = this.materialValuationObjectBindingSource.Add(_MaterialValuationObj);
                        SetDefaultValues(_MaterialValuationObj); 
                }
            }

            this.materialValuationObjectListObject.RefreshDataRows();
        }

        /*
         *  List<String> businesresidentsIDsFound = new List<String>();
                var SelectedRecObj = this.materialValuationObjectBindingSource.Current;
                this.materialValuationObjectBindingSource.MoveFirst();
                this.materialValuationObjectBindingSource.MoveLast();
                int totalrecords = this.materialValuationObjectBindingSource.Position;
                this.materialValuationObjectBindingSource.MoveFirst();
                for (int i = 0; i <= totalrecords; i++)
                {
                    if (this.materialValuationObjectBindingSource.Current != null)
                    {
                        MaterialValuationObject _MaterialValuationObjectObj = (MaterialValuationObject)this.materialValuationObjectBindingSource.Current;
                        if (_MaterialValuationObjectObj != null && _MaterialValuationObjectObj.MaterialID != null && GetFirstPart(_MaterialValuationObjectObj.MaterialID).Equals(MaterialID)
                            && _MaterialValuationObjectObj.BusinessResidenceID != null)
                        {
                            foreach (var item in this.BusinessResidenceList)
                            {
                                if (item.BusinessResidenceID.Equals(GetFirstPart(_MaterialValuationObjectObj.BusinessResidenceID)))
                                {
                                    businesresidentsIDsFound.Add(item.BusinessResidenceID);
                                }
                            }
                        }
                    }
                    this.materialValuationObjectBindingSource.MoveNext();
                }

                foreach (var item in this.BusinessResidenceList)
                {
                    var businesresidentsIDObj = businesresidentsIDsFound.Find(x => x.Equals(item.BusinessResidenceID));
                    if (businesresidentsIDObj != null && item.BusinessResidenceID.Equals(businesresidentsIDObj))
                        continue;
                    else
                    {
                        MaterialValuationObject _MaterialValuationObj = new MaterialValuationObject();

                        _MaterialValuationObj.CompanyID = DefaultCompanyID;
                        _MaterialValuationObj.SetOfBooksID = DefaultSetOfBooksID;
                        
                        MaterialInfo _MaterialInfoObj = MaterialInfoList.Find(x => x.MaterialID == MaterialID);
                        if (_MaterialInfoObj != null)
                            _MaterialValuationObj.MaterialID = _MaterialInfoObj.MaterialID + " - " + _MaterialInfoObj.Description;

                        _MaterialValuationObj.BusinessResidenceID = item.BusinessResidenceID + " - " + item.BusinessResidenceName;

                        int res = this.materialValuationObjectBindingSource.Add(_MaterialValuationObj);
                        SetDefaultValues(_MaterialValuationObj); 
                    }
                }
         */
        public void CreateAllMaterialObjectsInSAP()
        {
            ManageMaterialValuation _ManageMaterialValuationObj = new ManageMaterialValuation();
            this.materialValuationObjectBindingSource.MoveFirst();
            this.materialValuationObjectBindingSource.MoveLast();
            int totalrecords = this.materialValuationObjectBindingSource.Position;
            this.materialValuationObjectBindingSource.MoveFirst();
            for (int i = 0; i <= totalrecords; i++)
            {
                if (this.materialValuationObjectBindingSource.Current != null)
                {
                    MaterialValuationObject _MaterialValuationObjectObj = (MaterialValuationObject)this.materialValuationObjectBindingSource.Current;
                    if (_MaterialValuationObjectObj != null)
                    {
                        Boolean res = ValidateMaterialValuationObject(_MaterialValuationObjectObj, i+1);
                        if (res == false)
                            break;
                        else
                        {
                            if (_MaterialValuationObjectObj.IsValuationExistsInSAP == false)
                            {
                                Cursor.Current = Cursors.WaitCursor;
                                MaterialValuationBusinessResidanceInfo _MaterialValuationBusinessResidanceInfoObj = new MaterialValuationBusinessResidanceInfo();
                                _MaterialValuationBusinessResidanceInfoObj.CompanyID = GetFirstPart(_MaterialValuationObjectObj.CompanyID);
                                _MaterialValuationBusinessResidanceInfoObj.BusinessResidenceID = GetFirstPart(_MaterialValuationObjectObj.BusinessResidenceID);
                                _MaterialValuationBusinessResidanceInfoObj.MaterialID = GetFirstPart(_MaterialValuationObjectObj.MaterialID);

                                ResponseMessage _ResponseMessageObj = _ManageMaterialValuationObj.CreateMaterialValuationForBusinessResidance(_MaterialValuationBusinessResidanceInfoObj);
                                Cursor.Current = Cursors.Default;
                                if (_ResponseMessageObj.Res == true)
                                {
                                    _MaterialValuationObjectObj.IsValuationExistsInSAP = true;
                                    _MaterialValuationObjectObj.IsBusinessResidanceCreatedInSAP = true;
                                    _MaterialValuationObjectObj.BusinessResidenceSAPMessage = String.Empty;
                                }
                                else
                                {
                                    _MaterialValuationObjectObj.IsBusinessResidanceCreatedInSAP = false;
                                    _MaterialValuationObjectObj.BusinessResidenceSAPMessage = _ResponseMessageObj.responseMessage;
                                }
                            }

                            if (_MaterialValuationObjectObj.IsValuationExistsInSAP == true && _MaterialValuationObjectObj.IsPushedInSAP == false)
                            {
                                Cursor.Current = Cursors.WaitCursor;
                                ResponseMessage _ResponseObj = _ManageMaterialValuationObj.MaterialValuationInSAP(ref _MaterialValuationObjectObj);
                                Cursor.Current = Cursors.Default;
                                if (_ResponseObj.Res == true)
                                {
                                    _MaterialValuationObjectObj.IsPushedInSAP = true;
                                    _MaterialValuationObjectObj.PushStatusMessageFromSAP = _ResponseObj.responseMessage;
                                    _MaterialValuationObjectObj.PushDateTime = DateTime.Now;
                                }
                                else
                                {
                                    _MaterialValuationObjectObj.IsPushedInSAP = false;
                                    _MaterialValuationObjectObj.PushStatusMessageFromSAP = _ResponseObj.responseMessage;
                                }
                            }
                        }
                    }
                }
                this.materialValuationObjectListObject.RefreshDataRow(i);
                this.materialValuationObjectBindingSource.MoveNext();
            }
        }

        private void materialValuationObjectListObject_Change(Range targetRange, ListRanges changedRanges)
        {
            if (this.materialValuationObjectBindingSource.Current != null)
            {
                MaterialValuationObject _MaterialValuationObjectObj = (MaterialValuationObject)this.materialValuationObjectBindingSource.Current;
                if (_MaterialValuationObjectObj != null && _MaterialValuationObjectObj.MaterialID != null)
                {
                    SetDefaultValues(_MaterialValuationObjectObj);                    
                }
            }
        }

        private String GetFirstPart(String Value)
        {
            String str = Value.Substring(0, Value.IndexOf(" - ")).Trim();
            return str;
        }
    }

    public class UoMObject
    {
        public String Code;
        public String Value;
    }

    public class BusinessResidence
    {
        public String BusinessResidenceID;
        public String BusinessResidenceName;
    }
}
